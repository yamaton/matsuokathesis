% !Mode:: "TeX:UTF-8"
% !TEX root = __main.tex

\chapter{Statistics of the maximal Strehl ratio} % (fold)
\label{sec:statistics_calculations}

%% ============================================================
%%        Problem Statement 
%% ============================================================
%
\section{The Problem}
In this Appendix we'll discuss statistics of the minimum distance squared $W$ among independent and identically distributed (i.i.d.) random variables $Z_i$
\[
    W \equiv \min { Z_1^{(N)}, Z_2^{(N)}, \hdots , Z_M^{(N)}  }.
\]
Here $Z$ is deviation from the perfect coherence in terms of the Strehl ratio $R$.
\[
    Z \equiv 1 - R = 1 - \frac{1}{N^2} \, \abs{\sum_{k=1}^N e^{i (\Theta_k - \theta_{\mathrm{opt} \, k})}}^2 
\]
where $\Theta_k$ are i.i.d.\ uniform random angles: $\Theta \sim U(0, 2 \pi)$. [TODO: Explain what $\theta_{\text{opt}, k}$ is.]


Since the minimum is taken from large number $M$ of samples, ($M \sim 10^2$ in simulation $M \sim 10^7$ in reality), what matters is the lower tail of distribution of $Z$.  Such a small $Z$ is realized by angles accumulated about their mean $\delta \theta_0 = \arg\left( \sum_{j=1}^N e^{i \delta \theta_j } \right)$.  $Z$ may be interpreted as distance squared from the optimal point in $N$-dimensional torus space if we introduce zero-mean angle deviations by $\Delta \theta_k = \delta \theta_k - \delta \theta_0$ and $\sum_j \Delta \theta_j = 0$.  (Symmetry of global rotation allows the extra restriction of zero-mean angles.) Expanding Strehl ratio for small $\abs{\Delta \theta} \ll 1$, we have
\begin{align*}
    R &= \frac{1}{N^2} \abs{\sum_k^N \exp (i \Delta \theta_k) }^2 \\
      &= \frac{1}{N^2} \left[ \sum_k^N  \left(1 - \frac{1}{2} \Delta \theta_k^2 \right) \right] {}^2 
        + \frac{1}{N^2} \left( \sum_k^N \Delta \theta_k  \right) {}^2 \\
      &= 1 - \sum_k^N \Delta \theta_k^2 + O(\Delta \theta^4)\\
      &= 1 - Z 
\end{align*}
This is how ``distance squared'' (the concept used in old version of power scalability argument) is connected to Strehl ratio.





%% ============================================================
%%        Statistics
%% ============================================================
%
\section{Statistics}
[Comment: Is this an application of extreme value theory?]
Now, we'll compute statistics of Strehl ratio.~\footnote{Analogous calculation is presented in Siegman's  unpublished calculation\cite{SiegmanUnpublished} though he arrived at the importance of Strehl ratio heuristically.} If all angles are concentrated around the mean angle, you may say they are also located near the $N$-th sampled angle, $\theta_N$. Taking the difference from the $N$-th sampled point, we may utilize global rotation invariance of Strehl ratio and reduce the dimension by one: Strehl ratio is now computed from $(N-1)$ random variables.  Expanding all angles about $\theta_N$ we have
\begin{align*}
    Z &= 1-R \\
      &= 1-\frac{1}{N^2} \abs{1 + \sum_{k=1}^{N-1} \exp (i (\theta_k - \theta_N)) }^2 \\
      &= 1- \frac{1}{N^2} \left[ 1 + \sum_{j=1}^{N-1} \left(1 - \frac{1}{2}\delta \theta_j^2 \right) \right] {}^2
       - \frac{1}{N^2} \left[ \sum_{j=1}^{N-1} \delta \theta_j \right] {}^2   \\
      &= \frac{N-1}{N^2} \left( \sum_{j=1}^{N-1} \delta \theta_j^2  
          - \frac{2}{N-1} \sum_{j=2}^N \sum_{k=1}^{j-1} \delta \theta_j \delta \theta_k \right)
\end{align*}

Note that 
\begin{align}
      \sum_{i=1}^{N-1} x_i^2 +  2 a \sum_{i=2}^{N-1} \sum_{j=1}^{i-1} x_i x_j 
    = \begin{pmatrix}
          x_1 & \hdots & x_{N-1} 
       \end{pmatrix}
       \begin{pmatrix}
           1    &  a     & \hdots & a \\
           a    &  1     & \ddots & \vdots \\
         \vdots & \ddots & \ddots & a      \\
           a    & \hdots & a      &  1
    \end{pmatrix}
    \begin{pmatrix}
          x_1 \\ \vdots \\ x_{N-1}
     \end{pmatrix}
\end{align}
and this $(N-1) \times (N-1)$ symmetric matrix has eigenvalues 
$\lambda_1 = 1+(N-2)a, \lambda_2 = \hdots = \lambda_{N-1} = 1-a$.\cite{MatrixAnalysisTextbook}
Thus $P\{ Z  \leq  z \}$ may be interpreted geometrically as the probability to find a point within $(N-1)$-dimensional hyper-ellipsoid with radius $N \sqrt{z}$ along one principal axis and radius $\sqrt{N z}$ along $(N-2)$ principal axes.  Volume of $N$-dimensional ellipsoid is given by
\begin{equation*}
    V_N = \frac{\pi^{\frac{N}{2}}}{\Gamma \! \left[\frac{N}{2}+1\right]} \prod_{i=1}^N a_i
\end{equation*}
where $a_i$ is radius along principal axes.  Because the point is uniformly distributed in the space, volume ratio of the hyperellipsoid to the whole space gives the probability.  
\begin{align*}
    P\{Z \leq z\} 
      = \frac{V_{N-1}}{(2 \pi) {}^{N-1}} 
      = \frac{\pi^{\frac{N-1}{2}}}{(2 \pi) {}^{N-1}} 
        \frac{N^{N/2}}{\Gamma \! \left(\frac{N-1}{2}+1\right)} z^{(N-1)/2}
\end{align*}
Notice that this distribution function breaks for large $z$ since angles have been assumed small.
%% (begin)--------------------------------------------------
%%    ** density function is not needed for our argument
% The approximate density function of $Z$ is thus
% \begin{align*}
%     f_Z(z) \approx \d{}{z} P\{Z \leq z\} 
%            = \frac{N-1}{2}  \frac{\pi^{\frac{N-1}{2}}}{(2 \pi)^{N-1}} 
%                    \frac{ N^{N/2}}{\Gamma \! \left(\frac{N-1}{2}+1\right)} z^{(N-3)/2}
% \end{align*}
%% (end)--------------------------------------------------
% 
% 
% 
% For example, for $N=2$, $Z^{(N=2)}$ is represented by distribution function 
% \begin{equation*}   
%     f_{Z^{(2)}}(z) = f_{X^2}(z) = u(z) /(2 \sqrt{z})
% \end{equation*}
% where $u(x)$ is square function.
% \begin{equation*}    
%     u(x) =
%     \begin{cases}
%     1  \ \ & \text{if } 0 \leq z \leq 1 \\
%     0  \ \ & \text{otherwise}.
%     \end{cases}
% \end{equation*}




Now, let's consider the distribution of random variable $W$.  $P\{W \leq w\}$ is a complement of the case all $Z$'s are greater than $w$.  Thus, utilizing statistical independence, 
\begin{equation*}
    P\{ W \leq w \} 
    = 1 - \bigcap_{i=1}^M P\{ Z_i > w \} 
    = 1 - \left[ P\{Z > w\} \right] {}^M 
    = 1 - \left[ 1-  P\{Z \leq w  \}\right] {}^M
\end{equation*}

%% (begin)--------------------------------------------------
%%    ** density function is not needed for our argument 
% By taking derivative of the distribution function, density function is obtained.
% \begin{equation*}
%     f_W(w) 
%     = \d{}{w} P\{ W \leq w \} 
%     = M f_Z(w) \left[ 1 - P\{Z \leq w\} \right]^{M-1}
% \end{equation*}
%% (end)--------------------------------------------------
The expectation value of $W$ is thus,
\begin{equation*}
    E[W] 
    = \int dz \;  z \, f_W(z) 
    \approx \int_{0}^{1} z \d{}{z} P\{ W \leq z \}  dz
\end{equation*}
We have so far approximated assuming $W$ is distributed mostly for small $W$.  So the integration range over $[0, 1]$ should cover the most of the distribution.  Integration by part gives
%
\begin{align*}
    E[W] &\approx P\{ W \leq 1 \}  - \int_{0}^{1} P\{ W \leq z \}  dz \\
         %%% Not quite sure I shoud proceed along this approx.
         &\approx \int_{0}^{1} (1-P\{ Z \leq z \}) {}^M  dz 
\end{align*}
%
Because $P\{Z \leq z\} \ll 1$ for $z \in [0, 1]$ and $M \gg 1$, the integrand may be approximated as exponential form.  Then we eventually get an explicit form.
%
\begin{align}
    E[W] &\approx \int_{0}^{1} e^{-M P\{ Z \leq z \}} dz \notag \\
         &= \frac{2}{N-1} 
         \left[ g(M, N) \right] {}^{-\frac{2}{N-1}}
         \gamma \! \left(\frac{2}{N-1}, g(M,N)\right)
\end{align} 
%
where $\gamma(s,x)$ is lower incomplete gamma function: 
%
\begin{equation*}
    \gamma(s,x)=\int_0^{x} t^{s-1} e^{-t} dt.
\end{equation*}
%
And the function $g(M, N)$ is defined by
%
\begin{equation}
    g(M, N) = \frac{\pi^{\frac{N-1}{2}}}{(2 \pi) {}^{N-1}} 
              \frac{M N^{N/2}}{\Gamma \! \left(\frac{N+1}{2}\right)}.
\end{equation}

% !Mode:: "TeX:UTF-8"
% !TEX root = __main.tex


\chapter{Stability of single-mode CW states} % (fold)
\label{chap:stability_analysis}


%% ============================================================
%%       Stability Calculation
%% ============================================================
We perform local stability analysis of a single-mode CW solution in a multi-mode coupled fiber structure model. 
Namely, we calculate stability of the fixed-point solution \eqref{eq:generalCWsol} of
the multimode model \eqref{eq:EOM_coupled_fiber_structure}.

Element-wise computations of the Jacobian of \eqref{eq:EOM_coupled_fiber_structure} evaluated at a single-mode solution 
\eqref{eq:generalCWsol} are, after algebraic manipulations, given by the form
%
\begin{align*}
     \left.  \pd{E_i^{(\mu)}(t+T)}{E_k^{(\nu)}(t)} \right|_{\alpha}
  &= r \delta_{\mu \nu}
      \, e^{\bar{G}_i + i \phi_i^{(\mu)}} \mathcal{U}_{a i} \mathcal{U}_{a k} \\
  &= \delta_{\mu \nu} \frac{e^{i \phi_i^{(\mu)}}}{\sqrt{R^{(\alpha)}}}
      \, \mathcal{U}_{a i} \mathcal{U}_{a k} ,\\
%%
    \left.  \pd{E_i^{(\mu)} (t+T)}{G_k(t)} \right|_{\alpha}
  &= \delta _{i k}  \, r \sum_{m=1}^N e^{\bar{G}_i+ i \phi_i^{(\mu)}}  \mathcal{U}_{a i} 
       \mathcal{U}_{a m}  \bar{E}_m^{(\mu)} \\
  &= \delta_{\mu \alpha} \, \delta_{i k}  \, \bar{E}_i^{(\alpha)} ,\\
%%
    \left.  \pd{G_i(t+T)}{E_k^{(\mu)}(t)} \right|_\alpha
    &= \delta_{i k} \left[- 2\epsilon \left(1-e^{-2 \bar{G_i}}\right)  \bar{E}_i^{(\mu) *}\right] \\
    &= \delta_{\mu \alpha} \, \delta_{i k}  \left[- 2\epsilon  \left(1 -r^2 R^{(\alpha)}\right) \bar{E}^{(\alpha)*}  \right] , \\
%% 
    \left.  \pd{G_i(t+T)}{G_k(t)} \right|_\alpha
    &=  \delta_{i k} \left[ 1-\epsilon  \left( W \tau +1 +4 e^{-2 \bar{G_i}} 
        \sum_{\nu =1}^M  \abs{\bar{E}_i^{(\nu)}}^2\right)\right] \\
    &=  \delta_{i k} \left[ 1-\epsilon  \left( W \tau +1+4 r^2 R^{(\alpha)} \abs{\bar{E}^{(\alpha)}}^2\right)\right].
\end{align*}
%
After rearranging the element-wise calculations,  this $(2MN+N)$ by $(2MN+N)$-matrix Jacobian 
may be represented by $N \times N$-block submatrices $\{A^{(1)}, \cdots, A^{(M)}, B, C, D\}$,
%
\[
    J_\text{single mode} = 
    \begin{pmatrix}[0.6]
         A^{(1)} &       &     &       &     &       &        &           & \\
             &\ddots &     &       &     &       &        &           & \\
             &       & A^{(M)} &       &     &       &        &           & \\
             &       &     & A^{(1)*} &     &       &        &           & \\
             &       &     &       & \ddots &    &        &           & \\
             &       &     &       &     & A^{(M)*} &        &           & \\
             &       &     &       &     &       & A^{(\alpha)} &   0     & B   \\
             &       &     &       &     &       &   0    & A^{(\alpha)*} & B^* \\
             &       &     &       &     &       &   C    & C^*       & D
    \end{pmatrix}
\]
%
In the diagonal part, index $\mu$ of $A^{(\mu)}$ runs all values but $\alpha$ 
such that the series of $(M-1)$ matrices are aligned:  $A^{(1)}, \cdots, A^{(\alpha - 1)}, A^{(\alpha + 1)}, \cdots, A^{(M)}$.
Elements of the Jacobian's sub-block matrices are of the form,
\begin{align*}
     \left( A^{(\mu)} \right) {}_{i k} 
    &= \frac{e^{i \phi_i^{(\mu)}}}{\sqrt{R^{(\alpha)}}} \mathcal{U}_{a i} \mathcal{U}_{a k}, \\
      B_{i k} &= \delta_{i k} \, \bar{E}^{(\alpha)},  \\
     C_{i k}  &= \delta_{i k} \left[- 2 \epsilon \left(1 - r^2 R^{(\alpha)} \right) \bar{E}^{(\alpha) *} \right],  \\
     D_{i k} &= \delta_{i k} \left[ 1 - \epsilon \left(W \tau + 1+ 4 r^2 R^{(\alpha)} \abs{\bar{E}^{(\alpha)}}^2 \right) \right].
\end{align*}
%
A sub-block matrix $A^{(\mu)}$ has the spectral radius
\[
    \rho \left(A^{(\mu)} \right) = \Tr A^{(\mu)} = \sqrt{\frac{R^{(\mu)}}{R^{(\alpha)}}}.
\]
If the single-mode solution labeled $\alpha$ does not have the maximum Strehl ratio among 
the all realizations $\mu = 1, \ldots, M$,  the spectral radius of the matrix $A^{(\mu)}$ exceeds unity.
It follows that Jacobian has an eigenvalue whose magnitude is greater than unity, and therefore, 
the fixed point becomes unstable. 

If a fixed point is accompanied by the maximum Strehl ratio, 
all eigenvalues determined from the diagonal part lie inside of the unit circle in the Gaussian plane.
Hence the stability is to be determined from $3M \times 3M$ sub-block of the Jacobian.

In summary, local stability analysis has shown that all but one single-mode fixed points are locally unstable, 
and stability of the exception is uncertain due to the algebraic complication. 


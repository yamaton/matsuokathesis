% !Mode:: "TeX:UTF-8"
% !TEX root = __main.tex

\chapter{Polarization Control in Coherent Beam Combining}
\label{chap:polarization}
%
%%===========================================================
%%           Background / Experiment
%%===========================================================
%%
\section{Background}
\subsection{Is polarization control necessary?}
The importance of polarization adjustments in passive beam combining is controversial, 
as pointed out by Cao \emph{et al}\cite{Cao2009polarization}.
Many researchers report\cite{Simpson2002, Sabourdy2003, Shirakawa2002, Shirakawa2005} 
successful coherent combination in $N$ array elements with the installation of 
$(N-1)$ or more\footnote{Simpson\cite{Simpson2002} inserted a polarization controller in each of 
two arms in an $N=2$ coupled fiber setup and reported that ``a single adjustment is sufficient for 
data taken over several hours'' and observed degradation of coherence down to \SI{50}{\percent} combining 
efficiency by manipulating the polarizers. Sabourdy\cite{Sabourdy2003} inserted a single polarization 
controller in an $N=2$ system and reported that ``The combining efficiency optimized by adjusting the 
polarization controller at a given emission wavelength does not require additional adjustments at 
other wavelengths.''} polarization controllers while others claim\cite{Minden2004, 
Bruesselbach2005} that polarization control is unnecessary because of the self-organizing nature of the system.
More recently, Cao \textit{et al} claimed, based on their experiments\cite{Cao2009polarization}
that polarization adjustments are \emph{unnecessary} in Michelson-interferometer linear cavities,
a class which includes our coupled Fabry-Perot cavities, but they are \emph{necessary} in Mach-Zehnder cavities.    


\subsection{Curious case of a two-laser experiment}
Our unpublished experiments of two fibers in a Michelson-interferometer geometry
(Figure~\ref{fig:twolasers_schematic_polarization}) shows an interesting behavior: 
polarization adjustments in \emph{one} arm are required to obtain coherence in general,
but no adjustment is needed in a special case -- and in a strange manner. 
%
\begin{figure}[!h]
    \centering
    \includegraphics[width=13cm]{pdf/figure_twolasers_schematic_polarization.pdf}
    \caption{A system of two coupled fibers, identical to the one considered in Chapter~\ref{chap:dynamics}, 
    is used to investigate the importance of polarization control.}
    \label{fig:twolasers_schematic_polarization}
\end{figure}
%
As shown in upper two graphs of the Figure~\ref{fig:willray_coherence_vs_polarization}, 
completely coherent addition ($\omega=1$ in the graphs) is achieved 
when one of the polarization controllers is tuned.
When the other polarization is set to a certain angle giving perfect addition, however,
changes in the principal axis of polarization in the other arm does not have any effect on laser addition (bottom graph in Figure~\ref{fig:willray_coherence_vs_polarization}.)
%
%
\begin{SCfigure}[][!h]
    \centering
    \includegraphics[width=8cm]{pdf/figure_willray_coherence_vs_polarization.pdf}
    \caption{{\bf Kurt says:  In the caption and text you use $\omega$ but in the figure you label the axis $\varepsilon$.}  Addition efficiency is plotted against the azimuthal angle setting $\theta _1$
    of the polarization controller attached to arm \# 1.
    Generally, adjustment of $\theta _1$ is required to achieve coherence, $\omega = 1$ 
    (top and middle panels).
    However, when the polarization setting of the other arm $\theta_2$ is adjusted to give $\omega = 1$, the coherence persists even if $\theta_1$ is varied (bottom panel).}
    \label{fig:willray_coherence_vs_polarization}    
\end{SCfigure}
%
We will explain this curious experiment and clarify the role of polarization control 
in coherent beam combining, using the theoretical tools and numerical results developed in Chapter~\ref{chap:dynamics}.


%%===========================================================
%%           Geometry of Polarized Light
%%===========================================================
\section{Geometry of Polarized Light}
%
%-------------------------------------------
\subsection{Jones vectors and Bloch sphere}
%
Polarization of light is represented by the Jones vector $\vect{x}$,
a point on the $3$-sphere $S^3 = \left\{ \vect{x} \in \mathbb{C}^2, \abs{\vect{x}} = 1 \right\}$.
In component form,
%
\begin{align}
    \vect{x} = \left( E_x, E_y \right)
             = e^{i \gamma} \left(\cos \frac{\theta}{2},  e^{i \varphi} \sin \frac{\theta}{2} 
             \right)
     \label{eq:jones_vector}
\end{align}
%
where $\theta \in [0, \pi]$ and $\varphi \in [0, 2\pi)$ . 
$e^{i \gamma}$ is an overall phase factor and does not make any difference in physical state. 
Due to the irrelevance to the parameter $\gamma$, a state may be represented by a point $\vect{x'}$ in the projected space
$S^3 / \mathrm{U}(1)$, which is coset of $\mathrm{U}(1)$ in 3-sphere $S^3$.
And it is known that $S^3/\mathrm{U}(1)$ is isomorphic to $S^2$.

This Jones vector representation of polarized light is mathematically identical to quantum spin-$\frac{1}{2}$ state $\Ket{\psi}$.
\[
    \Ket{\psi} = \cos \frac{\theta }{2} \Ket{+}  + e^{i \varphi} \sin \frac{\theta}{2} \Ket{-}.
\]
Just as for the Jones vector, the overall phase factor is disregarded and $\Ket{\psi}$ and $e^{i \gamma} \Ket{\psi}$
are treated equivalent in many cases in quantum mechanics.

The geometrical representation of a state in the projected space $S^2$ is called 
the ``Poincaré sphere'' in optics, and the ``Bloch sphere'' in quantum two-state systems\cite{BornWolfTextbook}.
Extending the definition one can accommodate partially-polarized light (mixed states in quantum mechanics)
inside the sphere, but here we will limit ourselves in a purely polarized state. 
Letting the longitudinal direction of the fiber be the $z$ axis,
$\vect{x'}$ may be represented in sphare $S^2 \subset \mathbb{R}^3$ as follows
%
\begin{subequations}
    \begin{align}
        x &= \sin  \theta  \cos  \varphi = 2 \, \Realpart (E_x^* E_y) \, , \\
        y &= \sin  \theta  \sin  \varphi = 2 \, \Imagpart (E_x^* E_y) \, , \\
        z &= \cos \theta = \abs{E_x}^2 - \abs{E_y}^2 \, .
    \end{align}
    \label{eq:map_to_bloch_sphere}
\end{subequations}
%
Rotations about axes are given by
%
\begin{align}
    R_x(a) = 
    \begin{pmatrix}[0.8]
            \cos  \frac{a}{2}   & -i \sin \frac{a}{2} \\
         -i \sin  \frac{a}{2}   &    \cos \frac{a}{2} 
    \end{pmatrix},   
    \qquad %----- 
    R_y(b) = 
    \begin{pmatrix}[0.8]
        \cos \frac{b}{2}  & -\sin \frac{b}{2} \\
        \sin \frac{b}{2}  &  \cos \frac{b}{2} 
    \end{pmatrix}, 
    \qquad %-----
    R_z(c) = 
        \begin{pmatrix}[0.8]
            e^{-i c/2}  &  0         \\
            0           & e^{i c/2} 
        \end{pmatrix},
    \label{eq:three_dimensional_rotation}
\end{align}
%
which are actually exponential maps $R_i(\theta) = \exp (-i \sigma _i\theta  /2 )$ of generators called Pauli matrices: 
%
\begin{align*}
    \sigma_x =
    \begin{pmatrix}[0.6]
        0 & 1 \\
        1 & 0 
    \end{pmatrix}, 
    %-----
    \qquad 
    %-----
    \sigma_y =
    \begin{pmatrix}[0.6]
        0 & -i \\
         i & 0 
    \end{pmatrix},
    %-----
    \qquad 
    %-----
     \sigma_z &= 
    \begin{pmatrix}[0.6]     
        1 & 0 \\
        0 & -1
    \end{pmatrix}.        
\end{align*}
%
Although light polarization is graphically represented in the reduced space $S^2 \cong \mathrm{SU}(2)/\mathrm{U}(1)$, 
its transformation properties are described in $S^3 \cong \mathrm{SU}(2)$, a superset satisfying the group axioms.
We have to be aware of the working geometry because this juggling sometimes introduces confusion.
{\bf Kurt says:  We should re-work this last paragraph, once I understand exactly what point you're making.}



%-------------------------------------------
\subsection{Birefringence}
We now consider how the Jones vector $\vect{x}$ is transformed by birefringence. Birefringence 
introduces an axis-dependence of phase retardation along a fiber and is described by an operator 
$\mathcal{P}$: $S^3\to S^3$.  Common representation of $\mathcal{P}$ is given by two parameters $(\theta, \gamma)$,
where $\theta$ is the tilt angle of the principal axes of the polarization ellipse,
and $\gamma$ is the phase shift to the two components of light along the principal axes.
A polarization controller, illustrated in Figure~\ref{fig:polarization_controller_diagram}, 
works exactly the same manner as $\mathcal{P}$ with user-tunable parameters. In matrix form, it is given by
%
\begin{align*}
    \mathcal{P} (\theta, \gamma) 
    &= 
        \begin{pmatrix}[0.8]
            \cos \frac{\theta}{2} &  -\sin \frac{\theta}{2} \\
            \sin \frac{\theta}{2} &   \cos \frac{\theta}{2} 
        \end{pmatrix}
        %%
        \begin{pmatrix}[0.8]
             e^{- i \frac{\gamma}{2}}    &   0                \\
                           0   &   e^{i \frac{\gamma}{2}} 
        \end{pmatrix}
        %%
    \begin{pmatrix}[0.8]
          \cos \frac{\theta}{2}  &  \sin \frac{\theta}{2} \\
         -\sin \frac{\theta}{2}  &  \cos \frac{\theta}{2}
        \end{pmatrix} \\
    %%-----------
    &= 
    \begin{pmatrix}[0.8]
          \cos  \gamma -i \sin \gamma \cos \theta  & -i \sin  \gamma  \sin  \theta  \\
                          -i \sin  \gamma  \sin  \theta  & \cos \gamma + i \sin  \gamma  \cos  \theta 
    \end{pmatrix} \\
    %%
    &=
    \exp \left(- i \sigma_y \theta \right) 
    \exp \left(- i \sigma_z \gamma \right) 
    \exp \left(  i \sigma_y \theta \right)
\end{align*}
%
with $0 \leq \theta \leq 2 \pi$ and $- \pi < \gamma \leq \pi$. $\mathcal{P}$ is symmetric
$\mathcal{P}^T = \mathcal{P}$ reflecting the physical property that changes in polarization 
in counter-propagating waves are the same. 
Also, as the matrix form shows, a series of identical polarization controllers 
may be replaced by a single polarization controller.
\[
    \left[ \mathcal{P}(\theta, \gamma ) \right] {}^n = \mathcal{P}(\theta, n \gamma )
\]

%%-----------------------------------------------------------
%%  Polarization Controller Diagram
\begin{SCfigure}[][!h]
    \centering
    \includegraphics[width=8cm]{pdf/figure_polarization_controller_diagram.pdf}
    \caption{Diagram of a polarization controller. 
    Azimuthal angle and compression level giving birefringence are 
    adjusted via the fiber holder and actuator.}
    \label{fig:polarization_controller_diagram}    
\end{SCfigure}
%%-----------------------------------------------------------

The operator $\mathcal{P}$, with arbitrary choice of parameters, does not form a group because it fails to satisfy closure:
the product of two elements in $\mathcal{P}$ is not an element of $\mathcal{P}$ in general\footnote{A sequence of two 
different polarization controllers does form an $\mathrm{SU}(2)$ group, by the way.}.
This point is immediately confirmed by noting that an element $\mathcal{P}(\theta, \gamma)$ 
is a symmetric matrix but product of two symmetric matrices is not necessarily symmetric.
It is also the true that  $\mathcal{P}$ cannot transform a point $\vect{x} \in S^3$ to an arbitrary point in $S^3$ 
(i.e. the map is not surjective), simply because $S^3$ has three degrees of freedom while $\mathcal{P}$ has only two.


Plotting $\vect{y} = \mathcal{P}\vect{x}$ on the Poincaré sphere for varying two parameters $(\theta, \gamma)$ and 
for a given initial state $\vect{x}$, however, we find that the points $\vect{y}$ cover the entire sphere, 
as shown in Figure \ref{fig:bloch_sphere_coverage}. 
This implies that transitive relation holds when viewed in $S^2$; given three points $x, y, z$ related by 
\begin{align*}
    f( \vect{y} ) &= f \left( \mathcal{P} (\theta_1, \gamma_1) \vect{x} \right),   \\
    f( \vect{z} ) &= f \left( \mathcal{P} (\theta_2, \gamma_2) \vect{y} \right),    
\end{align*}
there exists $\mathcal{P}$ with proper choice of parameters $(\theta_3, \gamma_3)$ so that
\[
    f( \vect{z} ) = f \left( \mathcal{P} (\theta_3, \gamma_3) \vect{x} \right)
\]
where $f: S^3 \to S^2$ is a projection onto Poincaré sphere given in \eqref{eq:map_to_bloch_sphere}.
It is tempting to deduce a contradictory statement from the transitivity that the closure of $\mathcal{P}$ under multiplication holds,
\[
    \mathcal{P}(\theta_3, \gamma_3) = \mathcal{P}(\theta_2, \gamma_2) \mathcal{P}(\theta_1, \gamma_1).
\]
But this is \emph{incorrect} because the transitivity holds only in the projected space $S^2$.


Simple way to understand the nature of $\mathcal{P}$ is to use analogy to 3-D rotations.
With the help of \eqref{eq:three_dimensional_rotation}, the matrix is expressed by 
\[
    \mathcal{P}(\theta, \gamma) = R_y(\theta) R_z(\gamma) R_y(\theta) {}^\dagger = R_{z \text{, new}} (\gamma).
\]
and $R_{z \text{, new}}(\gamma)$ is considered as a rotation with angle $\gamma$ about a new axis 
$z_\text{new}$, which is tilted from the original $z$-axis by $y$-axis rotations in 3D space. 
$R_{z \text{, new}}(\gamma)$ can map an arbitrary point on the unit sphere to another.
But it cannot map an 3-D object into arbitrary orientaton since
the operator does not cover $SU(2) \cong S^3$. See Figure \ref{fig:airplane_rotation}.

%%-----------------------------------------------------------
%%  3-D rotation of airplane
\begin{SCfigure}[][!h]
    \centering
    \includegraphics[width=6cm]{pdf/figure_airplane_rotation.pdf}
    \caption{Rotation about $z_\textrm{new}$, an axis tilted from $z$ by $y$-rotation, 
    can transform the airplane to face any direction but it cannot transform the body
    into arbitrary orientation.}
    \label{fig:airplane_rotation}    
\end{SCfigure}
%%-----------------------------------------------------------




%%===================================================================
%%      Polarization Control
%% ====================================================================
\section{Polarization Control For Complete Coherent Beam Combination}
%
%% ---------------------------------------------------------
\subsection{Lowest-loss mode selection as a guiding principle}
%%
Inspired by the selection dynamics in coupled fiber structure presented in Chapter 
\ref{chap:dynamics}, we will discuss the importance of polarization control in coupled fiber systems
by assuming the lowest-loss selection rule still holds. Hence, we only deal with the statics of
the steady state identified by the lowest-loss rule, rather than the dynamics of polarized light.


%% ---------------------------------------------------------
%%
\subsection{Steady states in two coupled fiber lasers}
%%
We denote the transfer operator for a round trip of light in the upper-left arm by 
$\mathcal{T}_1$, in the lower-left arm by $\mathcal{T}_2$, and in the upper-right arm by $\mathcal{T}_R$
 (Figure~\ref{fig:polarization_twolaser_diagram}). 
 Also let $\vect{x_1}$ be the Jones vector of the right-going wave at the boundary of the 
upper-left arm and the coupler and $\vect{x_2}$ be the corresponding Jones vector at the boundary of the lower-left arm and the coupler. Assuming 
the coupler is not affected by the light's polarization state, we apply the coupling matrix to 
Jones-vector fields in the same manner as for scalar fields.  That is, the fields before and after the 
coupler, $(\vect{x_1},\vect{x_2})$ and $(\vect{y_1}, \vect{y_2})$, are related by
%%
\begin{equation}
    \begin{pmatrix}[0.7]
       \vect{y_1} \\
       \vect{y_2}
    \end{pmatrix}
    =
    \begin{pmatrix}[0.7]
         \cos p & i \sin  p \\
       i \sin p & \cos  p 
    \end{pmatrix}
    %-----------------
    \begin{pmatrix}[0.7]
        \vect{x_1} \\
        \vect{x_2}
    \end{pmatrix}.
    \label{eq:polarization_twolaser_coupling_matrix}
\end{equation}
%
%%-------------------------
%%  
\begin{figure}[!h]
    \centering
    \includegraphics[width=13cm]{pdf/figure_polarization_twolaser_diagram.pdf}
    \caption{Transfer matrices $\mathcal{T}_1$, $\mathcal{T}_2$, and $\mathcal{T}_R$ 
    are defined for a round trip of light in each arm.}
    \label{fig:polarization_twolaser_diagram}    
\end{figure}
%%--------------------------
With transfer matrices indicated in Figure~\ref{fig:polarization_twolaser_diagram}, the 
fields in steady state must satisfy the following relation,
%
\begin{align*}
    \begin{pmatrix}[0.7]
        \vect{x_1} \\
        \vect{x_2}
    \end{pmatrix}
    =
    \begin{pmatrix}[0.7]
        \mathcal{T}_1 & 0 \\
           0             &  \mathcal{T}_2
    \end{pmatrix}    
    \begin{pmatrix}[0.7]
        \cos p \;    I_2   & i \sin  p \; I_2  \\
       i \sin  p \;   I_2  & \cos  p  \;  I_2
    \end{pmatrix}    
    \begin{pmatrix}[0.7]
        \mathcal{T}_R & 0 \\
           0             &  0
    \end{pmatrix}
    \begin{pmatrix}[0.7]
        \cos  p  \; I_2    &  i \sin  p   \;  I_2   \\
       i \sin  p   \; I_2   &   \cos  p   \;  I_2  
    \end{pmatrix}
    \begin{pmatrix}[0.7]
        \vect{x_1} \\
        \vect{x_2}
    \end{pmatrix}    
\end{align*}
%
where $I_2$ is the $2 \times 2$ identity matrix. This equation simplifies to
%
\begin{align*}
    \vect{x_1} &= \cos p \; \mathcal{T}_1 \; \mathcal{T}_R \; ( \vect{x_1} \cos p + i \vect{x_2} \sin p ), \\
    %%-----
    \vect{x_2} &= i \sin p \; \mathcal{T}_2 \; \mathcal{T}_R \; ( \vect{x_1} \cos p + i \vect{x_2} \sin p ).
\end{align*}
%
Rearrangement of the equations yields 
$\vect{x_2}= i \tan p \; \mathcal{T}_2 \; \mathcal{T}_1^{-1} \vect{x_1}$.
Eliminating $\vect{x_2}$, we obtain an eigenvalue-like equation for $\vect{x_1}$, 
%
\begin{equation}
    \vect{x_1} = \mathcal{T}_1 \; \mathcal{T}_R
    \left( \cos^2 \! p \; \mathcal{I} - \sin^2 \! p \; \mathcal{T}_2 \; \mathcal{T}_1^{-1} \right) \vect{x_1}.
    \label{eq:polarization_twolaser_steadystate_field}
\end{equation}
%
This equation is ``eigenvalue-like'' because it is not a linear equation: 
the Jones-vector field $\vect{x_i}$ and the atomic laser gain $G_i$ are implicit
in the transfer matrices $\mathcal{T}_1$ and $\mathcal{T}_2$, and
they are also related by nonlinear equations for light-atom interactions. 
Under the bold assumption that the polarization of light does not affect the interaction, we would write down a function
\begin{equation}
    \norm{\vect{x_i}}^2 = f_i (G_i) \qquad \text{for} \quad  i = 1, 2
    \label{eq:polarization_twolaser_steadystate_gain}
\end{equation}
where $f_i(G)$ is a monotonically decreasing function $df_i/dG < 0$ and $f_i > 0$. 
$f_1$ and $f_2$ are treated differently so as to accommodate the case of unequal pumping of the fibers.

% -------
Following the idea of addition efficiency introduced in Section~\ref{sub:beam_combination_efficiency},
we introduce ``loss'' $L$ in the beam combination, which is the amount of light leaked to the
bottom-right arm in Figure~\ref{fig:twolasers_schematic_polarization}. 
From Equation~\eqref{eq:polarization_twolaser_coupling_matrix} and 
\eqref{eq:polarization_twolaser_steadystate_field}
 % ---
\begin{equation}
    L = \norm{i \; \vect{x_1} \sin p + \vect{x_2} \cos p}
      = \abs{\sin p} \; \norm{( \mathcal{I} + \mathcal{T}_2 \; \mathcal{T}_1^{-1} ) \vect{x_1}}
    \label{eq:polarization_twolaser_loss}
\end{equation}
% ---
Note that the loss is independent of $\mathcal{T}_R$, meaning that birefringence and polarization 
control in the upper-right arm does not affect the combining coherence.

With the lowest-loss principle, we are led to an optimization problem 
which minimizes the loss \eqref{eq:polarization_twolaser_loss} under the constraints 
\eqref{eq:polarization_twolaser_steadystate_field} and 
\eqref{eq:polarization_twolaser_steadystate_gain}. 
In addition to the system's autonomous tuning to the best mode, 
we have control over the polarization controllers and can tune them to provide optimal performance. 




%% ----------------------------------------------------------------
\subsection{Transfer matrices}
%%
The transfer operator is composed of translation, birefringence, amplification by stimulated emission, 
and loss by reflection. Translation may be regarded as an overall phase rotation of the Jones-vector 
state, so it can be expressed by a multiplicative factor $e^{ i \beta L}$ where $\beta$ is a propagation constant and $L$ 
is an optical path length. Birefringence in a fiber is equivalent to having a polarization controller 
$\mathcal{P}(\theta, \gamma)$ with uncontrollable fixed parameters $(\theta, \gamma)$. The loss of 
light is assumed to occur only at the reflecting end while other losses, such as transmission and 
connector loss, are not considered here. Partial reflection occurs only in the upper-right arm in the Figure~\ref{fig:polarization_twolaser_diagram}
so the loss can be represented by simple multiplication by a positive constant $r < 1$. Amplification is due to 
stimulated emissions in the doped fiber region in the upper-left and lower-left arms.  For simplicity 
of argument, amplification is taken to be constant and independent of the polarization state\footnote{Technically, polarization 
actually matters in reflection and amplification of light. Nevertheless, we (tentatively)
ignore any such contributions for the sake of simplicity and clarity of argument.}. With these simplifications, 
everything except the birefringence matrices are multiplication constants. Thus, the transfer matrices for the 
three arms are given by
%%
\begin{subequations}
\begin{align}
    \mathcal{T}_1 &= e^{G_1} e^{i \beta L_1} \prod_{i} \mathcal{P}_{i},  \\
    \mathcal{T}_2 &= e^{G_2} e^{i \beta L_2} \prod_{j} \mathcal{P}_{j}, \label{eq:polarization_explit_T2} \\
    \mathcal{T}_R &= r \, e^{i \beta L_R} \prod_{k} \mathcal{P}_{k}.
\end{align}
\end{subequations}
%%
Here $\mathcal{P}$ represents either intrinsic birefringence or 
insertion of a polarization controller, and a subscript means
a different set of parameters, {\it e.g.} $\mathcal{P}_{i} \eqdef \mathcal{P} (\theta _i, \gamma _i)$.
We don't adjust the optical path lengths $(L_1, L_2, L_R)$ 
but we will assume they are incommensurate in the following analysis.
    

%%=======================================================
%%     Poincare/Bloch sphere coverage
\begin{SCfigure}[][!h]
    \centering
    \includegraphics[width=8cm]{pdf/figure_bloch_sphere_coverage.pdf}
    \caption{Points $\mathcal{P}(\theta, \gamma) x$ are plotted on the Poincaré sphere
     with $x = (0.6, 0.8 e^{1.0 i})$ and varying $\theta$ continuously in 
     $[0, \pi)$ and $\gamma$ discretely with $\gamma = 0.1, 0.2, \dotsc, 1.5$.}
    \label{fig:bloch_sphere_coverage}     
\end{SCfigure}
%%=======================================================



%-----------------------------------------------------
\subsection{Complete beam combination in two fiber lasers}
\label{sub:polarization_twolaser_combining}
%
We will calculate the conditions for completely coherent combination within the 
Jones-vector field framework. The analogous scalar-field framework was presented 
in Section \ref{sub:complete_combining_solution}. 
Forrowing the lowest-loss mode selection presented in Chapter~\ref{chap:dynamics}, 
we compute the steady state solutions, 
%
\[
  \vect{x} = \mathcal{T}_1 \; \mathcal{T}_R 
  \left( \cos^2 \! p \; \mathcal{I} - \sin^2 \! p \; \mathcal{T}_2 \; \mathcal{T}_1^{-1} \right) \vect{x},
\]
%
such that the loss $L$ is minimized,
%
\[
    L = \abs{\sin p} \norm{\left( \mathcal{I} + \mathcal{T}_2 \; \mathcal{T}_1^{-1} \right) \vect{x}}.
\]
%
Inspired by the experimental observations we seek perfect addition
so that $L=0$. Then two equations must be satisfied:
%
\begin{subequations}
    \begin{align}
        \mathcal{T}_2 \; \mathcal{T}_1^{-1} \, \vect{x} = - \vect{x}  
        \label{eq:polarization_perfect_combining1} \\
        \mathcal{T}_1 \; \mathcal{T}_R \; \vect{x} = \vect{x}         
        \label{eq:polarization_perfect_combining2}
    \end{align}
    \label{eq:polarization_perfect_combining}
\end{subequations}
%
When two different operators share the same eigenvector $\vect{x}$, 
they must commute:
\[ 
    \left[ \mathcal{T}_2 \, \mathcal{T}_1^{-1},\, \mathcal{T}_1 \, \mathcal{T}_R \right] = 0. 
\]    
which may be rewritten in the form,
%
\begin{equation}
    \mathcal{T}_1 \, \mathcal{T}_R \, \mathcal{T}_2 = \mathcal{T}_2 \, \mathcal{T}_R \, \mathcal{T}_1.
    \label{eq:polarization_twolaser_commutation}
\end{equation}


Let's assume the following relation can be satisfied with proper tuning of 
a polarization controller $\mathcal{P}_a$ attached to arm 2:
\begin{equation}
    \mathcal{T}_R \, \mathcal{T}_2 = \mathcal{T}_2 \, \mathcal{T}_R = \alpha \mathcal{I}
    \label{eq:polarization_twolaser_commutation2}
\end{equation}
Substituting this back into \eqref{eq:polarization_perfect_combining1}, we immediately find $\alpha = -1$.
The explicit form of \eqref{eq:polarization_twolaser_commutation2} is
%
\begin{equation}
    r e^{G_2} e^{i \beta (L_2 + L_R)} \prod_j \mathcal{P}_j \prod_k \mathcal{P}_k = -\mathcal{I},
    \label{eq:polarization_twolaser_condition}
\end{equation}
%
which determines the polarization controller setting $\mathcal{P}_a$ 
%
\begin{equation}
    \mathcal{P}_a = 
    \left( \prod_{j=1}^{a-1} \mathcal{P}_j \right)^{-1} 
    \left( \prod_k \mathcal{P}_k \right)^{-1}
    \left( \prod_{j=a+1}^{a-1} \mathcal{P}_j \right)^{-1}.
    \label{eq:polarization_twolaser_controller_setting}
\end{equation}
%
With the property $\mathcal{P}^{-1} (\theta, \gamma) = \mathcal{P}(\theta, -\gamma )$, 
the right hand side of \eqref{eq:polarization_twolaser_controller_setting}
is treated as a product of $\mathcal{P}$'s of different parameters.


And, because of the closure property on the Poincaré sphere discussed in the previous section, 
this product is represented by a single element $\mathcal{P}$, 
enabling \eqref{eq:polarization_twolaser_controller_setting} to be satisfied. Therefore,
the assumption~\eqref{eq:polarization_twolaser_commutation2} is justified and the 
relation~\eqref{eq:polarization_twolaser_commutation} is made possible.
The equation~\eqref{eq:polarization_twolaser_condition} further requires constraints for
the gain and the propagation constant, namely
%
\begin{align}
    & G_2=-\log  r  \nonumber \\
    & e^{i \beta (L_2+L_R)} = -1  \label{eq:polarization_twolaser_phaserelation2R}
\end{align}
%
Substituting the expression for $\mathcal{T}_2$ into 
\eqref{eq:polarization_perfect_combining1} yields
%
\begin{align}
    G_1 = G_2 = - \log  r \\
    %%
    e^{i \beta (L_2 - L_1)} = -1 \label{eq:polarization_twolaser_phaserelation12}\\
    %%
    \left( \prod_i \mathcal{P}_i \right) 
    \left( \prod_j \mathcal{P}_j \right)^{-1} \vect{x} = \vect{x}
    \label{eq:polarization_twolaser_eigenequation}
\end{align}
%
The gain values agree with the scalar-field dynamical model.
The phase relation is also autonomously satisfied by the system due to 
the lowest-loss mode selection rule.  Here we are assuming $(L_1, L_2, L_R)$ are incommensurate so both 
\eqref{eq:polarization_twolaser_phaserelation2R} an \eqref{eq:polarization_twolaser_phaserelation12}
are satisfied simultaneously and quasi-periodically to arbitrary precision. 
\eqref{eq:polarization_twolaser_eigenequation} shows the eigenvector $\vect{x}$ is 
determined from birefringence in arms 1 and 2 while the eigenvalue is fixed to 1.
Thus, even if the polarization controller attached to arm 1 is varied, 
complete addition is retained even though the eigenvector varies correspondingly.
This explains the experimental observation posed initially, {\it i.e.} that lossless addition 
becomes independent of one of the polarization controllers (bottom of Figure~\ref{fig:willray_coherence_vs_polarization}). 


When the polarization controller in arm 2 is \emph{not} adjusted to satisfy the relation 
\eqref{eq:polarization_twolaser_commutation}, complete addition is still achieved by making an 
operator equality $\mathcal{T}_2 = -\mathcal{T}_1$. Due to the closure property again,
this operator equality is always possible. Or, we can adjust the polarization controller in arm 1 to 
cancel out the birefringence in arms 1 and 2.  But the controller setting in arm 1 is now 
dependent on the polarization in arm 2. Hence, perfect coherent beam combination is lost as one varies the 
controller in the other arm. This explains the other experimental observation, {\it i.e.} 
that combining efficiency is dependent on the polarization setting (top and middle graphs in 
Figure~\ref{fig:willray_coherence_vs_polarization}).


%%---------------------------------------------------------
%%
\subsection{Polarization control in larger array}
%
For $N=2$ we have confirmed our experimental observations and conclude that only one polarization 
controller is necessary to achieve coherent beam combining. Then how many polarization controllers 
are needed in larger arrays? A straightforward extension of our argument suggests that $(N-1)$ polarization controllers are necessary for an array of $N$ linear fibers to achieve coherent 
combining in a single-feedback array. Let $c_{ij}$ be the $(i,j)-th$ element of the coupling matrix and let the $a^{th}$ fiber be the sole reflective fiber.  Then, analogously to Equation~\eqref{eq:n2_steady_state_field}, \textbf{--Yamato: wrong equation link!--} the field $\vect{x}$ 
satisfies the relation
%
\[
    \vect{x_n} = \sum_{i} \mathcal{T}_n \, c_{ni} \, 
                 \mathcal{T}_R \, \delta_{ia} \sum_{j}\delta_{ij}  \sum_{k} c_{jk} \vect{x_k},
\]
%
which leads to 
%
\[
    \frac{1}{c_{na}} \mathcal{T}_n^{-1}  \vect{x_n}  =  \mathcal{T}_R  \sum_{k} c_{ak} \vect{x_k}.
\]
%
Rewriting the steady-state equation with 
$\vect{x_n} = \frac{c_{n a}}{c_{1a}} \, \mathcal{T}_n \, \mathcal{T}_1^{-1}  \, \vect{x_1}$, 
we have
%
\[
    \vect{y} = \mathcal{T}_R \sum_{k} ( c_{ak}  c_{ka} \mathcal{T}_k ) \, \vect{y}
\]
%
where the new vector $\vect{y}$ is defined by $\vect{y} \eqdef \mathcal{T}_1^{-1} \vect{x_1}$. 
The loss at the $i^{th}$ angle-cleaved end ($ i \neq a$) is
%
\[
    L_i \eqdef \norm{\sum_{k} c_{ik} \vect{x_k}}
         = \frac{1}{\abs{c_{1a}}} 
           \norm{ \sum_{k} ( c_{ik} c_{ka} \mathcal{T}_k ) \; \vect{y} } \qquad \text{for} \quad  i \neq a
\]
%
Inspired by the arguments used in our scalar-field treatment (Section~\ref{sub:complete_combining_solution}) 
and in the case of two polarized fibers (Section~\ref{sub:polarization_twolaser_combining}), we see that the following 
operator equality must hold in order that the steady state solution has zero loss:
%
\[
  \mathcal{P}_{1 \ \mathrm{all}} = \mathcal{P}_{2 \ \mathrm{all}} = \dotsb = \mathcal{P}_{N \ \mathrm{all}}
\]
%
where $\mathcal{P}_{i\ \text{all}}$ is the product of birefringence effects $\mathcal{P}$ in fiber 
arm $i$.  To satisfy this equality requires $(N-1)$ polarization controllers. It also requires
that phase relation satisfy 
\[
    e^{ i \beta L_k} c_{ka} = c_{ka}^*  \qquad \text{for all } k = 1, \dotsc, N
\]
in the same way as in the scalar-field calculation in Section~\ref{sub:complete_combining_solution}. 
As long as lengths $L_1, L_2,  \dotsc, L_N$ are incommensurate, 
the equalities are again quasi-periodically satisfied to arbitrary precision with the proper choice of $\beta$.
As $N$ grows, however, convergence becomes slower due to a vernier effect, 
which was discussed earlier in the section on power scaling (Section~\ref{sec:power_scaling}).



%%----------------------------------------------------
%%
\subsection{Conclusion}
%%
We have successfully explained our experimental observations involving coherent combining and polarization controller settings
in terms of the Jones-vector extension of the laser model and the 
application of the lowest-loss mode selection rule developed in the scalar field description.
Our argument was further extended to larger arrays and we conclude that $(N-1)$
polarization controllers are required to achieve perfect combining in an $N$-element fiber array in a Michelson-interferometer configuration.

Our conclusion is in agreement with experiments by Shirakawa \emph{et al}\cite{Shirakawa2005} and Sabourdy \emph{et al}\cite{Sabourdy2003}.
Beam combination without polarization control as claimed by Cao \emph{et al}\cite{Cao2009polarization} 
and the HRL research group\cite{Bruesselbach2005, Minden2004} cannot be explained within our framework, however.
Further physical processes such as a polarization-dependent gain medium
would be needed to explain these autonomous phenomena.

% !Mode:: "TeX:UTF-8"
% !TEX root = __main.tex

%% =====================================================================
%% Thesis --- Introduction
%%
%%     2013-06    moved to bitbucket for revision management
%%     2013-06-20 totally revised
%%	2013-06-14	
%%	2012-11-09 modified	
%%     2010-10-20 started
%% =====================================================================

\chapter{Introduction}
\label{chap:intro}

%% =========================================================
%% # High Power Laser
%%
%% - power 
%% - beam quality
%% - radiance (brightness)
%% - robustness
%%
%% array 
%%    CBC and WBC **increases** radiance while incoherent addition does not [Fan, Leger].
%%    But I'm not going to discuss wavelength beam combining (WBC). Just mention WBC in 
%%    context of scaling of radiance. 
%%
%%  Source: Fan2005, Richardson2010
%% =========================================================
%%
\section{High Power Lasers}
Research and development of high-power lasers is moving toward improvement of power, 
beam quality, radiance, efficiency, and robustness. Power is net energy per unit time emitted as a laser beam.
Beam quality is a measure of the beam divergence during the propagation in free space and 
evaluated in dimensionless $M^2$.\footnote{Dimensionless $M^2$ is defined by the beam parameter product
normalized by that of the ideal Gaussian beam of the same wavelength. 
And the beam parameter product is computed from the product of the beam radius and the divergence angle.
Information of beam radius, in addition to the divergence angle, is necessary because
that determines the amount of unavoidable beam divergence due to diffraction.}  
Radiance, also called ``brightness'' in the laser community, is a combined measure
of field strength and beam divergence, in units of ``power per unit area per solid angle'',
or \si{\watt\per\meter\squared\per\steradian}. A robust system operates steadily without external control in a disturbing 
environment\footnote{From a dynamical-systems view, robustness is the size of 
parameter space for existence of an attractor, and/or the size of basin of attraction.}.

A high-power and high-quality beam is required for directed-energy applications, 
such as industrial cutting and wielding, energy transmission in space\cite{Komurasaki2005}, 
laser weapons\cite{Sheehan2010}, and laser-powered propulsion\cite{Pakhomov2000}.
Good beam quality minimizes divergence loss during transmission.
Chemical lasers, such as oxygen iodine lasers and deuterium fluoride lasers,
are often employed for directed-energy purposes 
because of the relative ease of high-power generation. 
Researchers have achieved up to ``Megawatt-class'' continuous-wave (CW) lasers, 
as of 2009, with beam quality close to the ideal limit\cite{Sprangle2009}.

A high-radiance beam is needed to ignite fusion reactions\cite{Kodama2002} and 
possibly to accelerate particles in a next-generation high-energy particle accelerator\cite{Mourou2013}. 
Field strength, instead of total power, is the key for these purposes.

%--- Fiber starts here
The fiber laser is one of the best platforms for both high power and high beam quality
because of its built-in waveguide and ease of heat removal due to large surface-volume 
ratio\cite{Richardson2010}. 
Thermal beam distortion, however, sets limits to the radiance. 
Single-mode (single-transverse mode) fibers, though easy to produce the best beam quality, 
suffer from this thermal effect the most because the core area is small and heat is accumulated.
High-power fiber lasers are therefore operated in array form to scale up power and radiance.
Arrays also offer better energy efficiency because a collection of moderately-powered 
laser sources is more efficient than a single high-powered source. 


%% =========================================================
%%       Incoherent and Coherent Beam Combining
%% =========================================================
\section{Why Combine Beams Coherently?}
Incoherent combination, or a bunch of collimated beams without phase control, 
can scale power proportional to the number $N$ of array elements. 
Good beam quality with robust operation is also attainable with incoherent 
combining\cite{Sprangle2009}. Indeed, the most powerful fiber laser as of 2009 came from 
this incoherent scheme, reaching \SI{10}{\kW}\cite{IPG-Photonics2009} in single-transverse mode.


Then what is the motivation for combining beams coherently?
The answer lies in the scaling of radiance: when lasers are coherently combined,
radiance scales up in proportion to the number of array elements.
In contrast, incoherent combining \emph{cannot} produce radiance of light 
greater than its constituent elements\cite{FanBookChapter}.


Limit of radiance for incoherent sources may be best understood from 
examples\footnote{The ``brightness theorem''\cite{LegerBookChapter} 
backs up the idea of conservation of radiance, though it is typically discussed within ray optics.}. 
Imagine a collection of laser pointers. When they are beamed in parallel
as in Figure~\ref{fig:incoherent_sources_parallel}, overall lasers may be treated 
as a single light source with a large diameter. 
The total power scales up with $N$, but both the power density and beam quality 
are still the same as those of a single source. If beams are set to focus at a point 
as in Figure~\ref{fig:incoherent_sources_targeted}, the power-density scales up at the focal point 
for sure but beam divergence is also inevitably increased, yielding zero increase in radiance, again.

%%========================================================
%% Stickmen illustration of incoherent addition of light
\begin{SCfigure}[][h!]
  \begin{subfigure}[b]{3cm}
      \includegraphics[width=\textwidth]{pdf/figure_incoherent_sources_parallel.pdf}
      \caption{Parallel}
      \label{fig:incoherent_sources_parallel}    
  \end{subfigure}
  %%
  \begin{subfigure}[b]{3.3cm}
      \includegraphics[width=\textwidth]{pdf/figure_incoherent_sources_targeted.pdf}
      \caption{Targeted}
      \label{fig:incoherent_sources_targeted}    
  \end{subfigure}
  \caption{Stick-men illustration of incoherent beam addition:
           even if people arrange lasers pointers such that beam are
           (a) in parallel or (b) in targeted to a point, radiance of the net beam 
           never exceeds the one of a single beam.}
\end{SCfigure}
%%========================================================



\section{Active and Passive Techniques}
To add multiple laser elements coherently we need to tune the individual optical phases to match them. 
This technique is already in practical use in radio-frequency domain as phased-array radars,
but the optics application is more difficult because the wavelengths are far shorter: 
lasers are in wavelengths scale $\lambda \sim \SI{1}{\um}$ while radars are 
of $\lambda \sim \SI{10}{\cm}$.


%% Active phase control
%%     Anderegg2006	... good example for active phase control
%%     Shay2007	  	... improvement by removing the need of reference beam
One straightforward approach for coherent addition is to install phase modulators 
to each light path and control them individually via some scheme involving sensors and electronic 
feedback circuits. We call such techniques ``active'' because the phases are directly 
manipulated. This architecture has produced powers as high as \SI{470}{\W}\cite{Anderegg2006}.
The innate drawback of active phase control is the complexity of the architecture 
due to integration of high-speed detectors and signal-processing units. 
The complexity even grows with the array size.


%% Passive phase control
%%      Fan2005      ... review of both coherent beam combinig and wavelength beam combining
%%      Corcoran2009 ... review of coherent beam combining
Passive phase control, in contrast, attempts to match phases 
by selectively activating collective modes\cite{Fan2005, Corcoran2009}. Spatial filtering or coupling 
(e.g.\ tapered fused coupler, Talbot cavity, self-Fourier cavity) is typically 
introduced to achieve synchronized excitation of ``phase-locked''\footnote{Special care 
must be taken in reading literature.  People in the
beam-combining community often use the terms ``phase locking'' or ``mode locking'' 
to denote successful phase control of waves with the \emph{same frequency}. 
In contrast, phase- or mode-locking typically refers to pulse generation technique
by adjusting the phase of waves with \emph{different frequencies}, 
as found in the standard laser textbook\cite{SiegmanTextbook}. 
Hence, ``phase locking'' in the context of  
beam combining does not necessary imply pulse generation.} modes. 
The passive scheme offers simpler architecture without phase modulators and sensors. 
With the self-organizing nature, it should also offer more robust operation.



%% ===========================================================
%%       Recent Breakthrough with Fiber Lasers Array
%% ===========================================================
\section{Passive Coherent Combining of Fiber Lasers}
Passive coherent beam combining of lasers has been of a long-standing research 
interest, especially with semiconductor laser arrays\cite{BotezBookChapter}. 
Experiments, however, have shown that coherent addition is 
successfully achieved with fiber lasers\cite{Shirakawa2002, Simpson2002}, 
even without careful control of parameters\cite{Bruesselbach2005} 
such as optical path lengths and polarization.

% --- wow! 
These successes were surprising because diode lasers, the major platform before fibers,
were struggling for a long time with limited successes\cite{Fan2005}.
Moreover, fiber lasers are in some sense more ``complex''
since (long) fibers contain much more active oscillators (i.e.\ axial modes) than diode lasers do. 
It was counter intuitive that phase matching was easily done 
in fiber laser arrays despite their far larger number of degrees of freedom\footnote{This paradoxical 
situation reminds me of the words of a samurai-sword master in a fiction\cite{Shigurui}.
\begin{quote}
  If one wishes to gain, he must first give. \\
  If one wishes to clench down, he must first loosen his grip. \\
  And so\ldots, if one wishes to open, he must first close.
\end{quote}
}.


% ---------
Even more unclear to me personally is current understanding of passive coherent beam combinaton itself.
It seems tacit knowledge like ``lower-loss mode excitation'', modes suffering less loss are activated more,
is common among laser researchers without solid explanation.
% -------- Merged Kurt's suggestion
From the perspective of a physicist, the state of existing theory on coupled laser arrays is somewhat primitive.  
Most theories\footnote{An exception is the analysis by Basov \textit{et al}\cite{Basov1965}
that treats two diffraction-coupled lasers with neoclassical equations\cite{SiegmanTextbook}.
They have identified that synchronization gets stable when the loss 
(i.e.\ coefficient of the dissipative term in the oscillators equation) is lower than the uncoupled case.}
are based on ``cold-cavity analysis'' that treats the laser system as a hollow cavity with undressed, dissipative modes.
This approach in effect treats the nonlinear dynamical system as a collection of damped linear oscillators.
%---------

In fact, even the broadly accepted usage of ``modes'' and ``loss'' in this literature 
are not well defined, or they are simply context-dependent.
Despite these fundamental limitations, the spatial filtering techniques, mentioned above, came from this 
understanding of the lower-loss activation mechanism and in fact they work fine in practice.
 
Although our ultimate goal involves understanding of passive coherent beam combining in general, 
we are going to focus on two specific schemes illustrated in Figure~\ref{fig:combining_diagram}.
One is an all-in-fiber addition approach\cite{Kozlov1999} that combines light within fiber array; 
the other is a multi-emission approach that combines coherent beams in the far field\cite{Ray2012Unpublished}.
%
%%========================================================
% Coherent combining Sketches
\begin{figure}[h!]
  \centering
  \begin{subfigure}[b]{7cm}
     \includegraphics[width=\textwidth]{pdf/figure_combining_diagram_passive_single.pdf}
     \caption{Single-output scheme}
  \end{subfigure}
 %
  \begin{subfigure}[b]{7cm}
      \includegraphics[width=\textwidth]{pdf/figure_combining_diagram_passive_multiple.pdf}
      \caption{Multi-output scheme}
  \end{subfigure}
  \caption{Approaches for passive coherent beam combining (Retrieved from \cite{Fan2005}).
  (a) is also called all-in-fiber scheme, which combines beams within fibers. 
  (b) produces multiple in-phase beams so that they combine coherently in far field.}
  \label{fig:combining_diagram}
\end{figure}
%%========================================================
All-in-fiber and far-field combinations have exclusive pros and cons.
All-in-fiber combination is simple in the sense beam collimation is not required. 
Output power is, however, limited by thermal distortion in fiber-core media 
because the combined light must go through a fiber. 
Far-field combination has the exact opposite nature and collimated beams are superposed in space.
Hence all-in-fiber combination is well suited for producing moderate-power beams and
far-field combination is for higher-power beams.


%\section{Structure of the Thesis}
\section{About the Thesis}
Chapter~\ref{chap:laserphysics} reviews the physics of the relevant optical and laser components 
used to create a dynamical model for coherent beam combining in Chapter~\ref{chap:models}. 
The dynamical model is used to discuss an all-in-fiber coherent combining in Chapter~\ref{chap:dynamics}.
The importance of polarization control is discussed later in Chapter~\ref{chap:polarization}.
The far-field beam addition motivated by weak-link synchronization\cite{Tsygankov2006}
is attempted in Chapter~\ref{chap:weaklink}. 
Chapter~\ref{chap:coupler} is a little off the track but it covers a potentially 
important aspect of laser arrays, namely group theory and algorithmic design 
of composite coupler construction, which are of both mathematical and experimental interest.

Discussions in Chapter~\ref{chap:laserphysics} is a patchwork of existing work.
Most discussions afterward (from Chapter~\ref{chap:models} to Chapter~\ref{chap:coupler})
are original unless cited, although I am always on the shoulders of giants and
helped enormously by my advisor and collaborators.
An exception is Section~\ref{sec:power_scaling} where I have unintentionally 
reproduced the Siegman's calculations\cite{SiegmanUnpublished}. 
The experimental data and their descriptions in Chapter~\ref{chap:weaklink} are 
provided from my collaborator, Dr.\@ Will Ray.

% !Mode:: "TeX:UTF-8"
% !TEX root = __main.tex

\chapter{Far-Field Beam Addition in Inhomogeneously Pumped Coupled Lasers}
\label{chap:weaklink}
%% ---------------------------------------------------------
%%              Introduction
%% ---------------------------------------------------------
\section{Synchronization of Inhomogeneous Oscillators}
%
Typically, the more homogeneous the oscillator elements, the easier it is to synchronize them.
This trend is well exemplified by the Kuramoto phase-oscillator model\cite{Strogatz2000}.
In the Kuramoto model, identical oscillators are synchronized with any nonzero coupling while 
only a subset of non-identical oscillators (differing with respect to their natural frequencies) synchronize, with the more detuned oscillators being excluded from the synchronized subset.  The fiber lasers discussed in Chapter~\ref{chap:dynamics} are fundamentally different from phase
oscillators, in that the amplitude dynamics dominates.  Even so, homogeneity of lasers within an array, {\it e.g.} with respect to the fiber pump rates,
is still advantageous for complete coherence of beam combination (Section~\ref{sub:complete_combining_solution}).

Tsygankov and Wiesenfeld, however, suggested\cite{Tsygankov2006} that a new type of synchronization
of coupled lasers is possible by exploiting parameter mismatches of the oscillator elements. They
took an array of Hopf oscillators, coupled them in a special geometry such that active (over-threshold)
elements sandwich inactive (under-threshold) elements, and achieved robust synchronization.
This ON-OFF-ON series geometry is illustrated in Figure~\ref{fig:weaklink_geometry} with 37 fibers
 (24 active + 13 inactive elements) bundled in parallel. This type of synchronization, which they called ``weaklink synchronization'',
 is expected to exist in various types of coupled oscillators where amplitude dynamics is crucial.
%
\begin{SCfigure}[][!h]
    \centering
    \includegraphics[width=6cm]{pdf/weaklink_geometry.pdf}
    \caption{In a theoretical demonstration of weaklink synchronization,
    37 nearest neighbor coupled oscillators are arranged such that
    super-threshold Hopf oscillators (circles in black) are sandwiched between
    sub-threshold Hopf oscillators (circles in gray).  
    Reproduced from \cite{Tsygankov2006}.}
    \label{fig:weaklink_geometry}
\end{SCfigure}
%

Inspired by this idea, we explore a new type of multi-output scheme with inhomogeneously pumped lasers,
such that beams are combined coherently in the far field.  We choose three fibers as the simplest realization
of the ON-OFF-ON format: the outer lasers are turned “ON” by using a pump rate greater than the lasing threshold value, 
and the middle one is turned “OFF” by using a pump rate less than the threshold amount.
To fully investigate the behavior of inhomogeneous elements, different levels of optical loss are assigned to the fibers.
Note that the present system is different from the all-fiber beam additions in Chapter~\ref{chap:dynamics}
in that it attempts to exploit multiple optical feedback (i.e.\ multiple reflections) to explore
coherent beam combination in the full phase space.



%% =========================================================
\section{Experiment}
%% =========================================================
%%
%% ---------------------------------------------------------
\subsection{Setup}
A schematic diagram of the three-laser array is shown in Figure~\ref{fig:weaklink_schematic}.
%
\begin{figure}[h!]
    \centering
    \includegraphics[width=15cm]{pdf/experiment_schematic_diagram.pdf}
    \caption{Schematic diagram of the three coupled laser array.  The oval labeled 
    80/20 is a two-by-two optical coupler allowing \SI{20}{\percent} interchange of light.
    A loop mirror consisting of a 50/50 coupler and a looped piece of fiber
    works effectively as a perfect mirror.
    WDM denotes a wavelength-division multiplexer introducing light from a pumping laser into the cavity.
    POL is a polarization controller.
    TAP is a coupler which extracts a small fraction of the laser signal for observation.
    FLAT CLEAVE is a flat-cleaved endcap.}
    \label{fig:weaklink_schematic}
\end{figure}
%
%% Extracted from “ the weak-link paper”
The laser elements have ytterbium-doped regions pumped by grating-stabilized \SI{976}{\nm} diode lasers.
A fiber loop mirror spliced onto one end of each doped fiber provides nearly \SI{100}{\percent}
broadband back reflection; a flat cleave on the other end reflects only \SI{4}{\percent} of the generated
light. An asymmetric coupling is formed by concatenating two $2 \times 2$
fused fiber couplers with an 80/20 power-splitting ratio.
The three output facets are aligned in a grooved substrate with a
spacing of \SI{250}{\um}; a CCD camera captures the far-field interference pattern at a frame rate of
\SI{77}{\Hz}. Tap couplers are placed in each of the output fiber arms to monitor the individual powers
and optical spectra. This array emits over a \SI{3}{\nm} bandwidth near \SI{1030}{\nm} and reaches a lasing
threshold once \SI{40}{\mW} of pump power is applied to any one of the elements.

The middle laser pump is tuned from \numrange{0}{2.5} times lasing threshold while the outer lasers are
fixed at \SI{10}{\percent} above threshold. We investigated this inhomogeneously pumped system with homogeneous
and inhomogeneous loss. Uneven loss is realized by applying additional cavity loss to the middle
fiber by compressing a tight coil of fiber so that over \SI{90}{\percent} of the incoming light is lost.


%% ---------------------------------------------------------
\subsection{Inhomogeneity of laser elements and coherence}
Coherence of the three laser beams is measured experimentally by the visibility $V$ of fringes of the far-field interference pattern.
Here, the visibility $V$ is determined from adjacent local maximum and minimum of irradiance\cite{HechtTextbook}.
\[
  V = \frac{I_{\max} - I_{\min}}{I_{\max} + I_{\min}}.
\]
The output power of the three lasers and the average visibility is shown in
Figure~\ref{fig:experiment_visibility} for homogeneous and inhomogeneous loss.
%
%  Visibility vs Pump
\begin{figure}
    \centering
    \begin{subfigure}[h]{7cm}
        \centering
        \includegraphics[width=5cm]{pdf/experiment_visibility_EvenLosses.pdf}
        \caption{Even loss}
    \end{subfigure}
    %%
    \begin{subfigure}[h]{7cm}
        \centering
        \includegraphics[width=5cm]{pdf/experiment_visibility_AddedLoss.pdf}
        \caption{Uneven loss}
    \end{subfigure}
    \caption{Average visibility vs middle laser pump level from the experiment.
    Outer laser pumps are fixed at $1.1 \times$ threshold.
    Uneven losses indicates the application of additional heavy losses in the middle fiber, as described in the text.}
    \label{fig:experiment_visibility}
\end{figure}
%
In both homogeneous and inhomogeneous cases, the three laser beams have different powers from each other; their individual values also depend on the pump level of the middle
fiber, as shown in Figure~\ref{fig:experiment_power}. When the losses are even, the output power from the middle
fiber is smaller than the power from outer fibers when the middle fiber is under-pumped, and the
power from the middle fiber exceeds the others when the middle fiber is over-pumped. When the losses are uneven, the output
power emanates mainly from two outer fibers regardless of the pump level.
%
%
% -------------------------------
%   Power vs Pump
\begin{figure}
    \centering
    \begin{subfigure}[h]{7.5cm}
        \centering
        \includegraphics[width=7cm]{pdf/experiment_PowerVsPump_EvenLosses.pdf}
        \caption{Even loss}
        \label{fig:experiment_visibility_EvenLosses}
    \end{subfigure}
    %%
    \begin{subfigure}[h]{7.5cm}
        \centering
        \includegraphics[width=7cm]{pdf/experiment_PowerVsPump_AddedLoss.pdf}
        \caption{Uneven loss}
        \label{fig:experiment_visibility_AddedLoss}
    \end{subfigure}
    \caption{Output powers vs middle laser pump.
        Outer laser pumps are fixed at \SI{10}{\percent} above the lasing threshold.
        Uneven losses indicates additional heavy losses in the middle fiber.}
    \label{fig:experiment_power}
\end{figure}
%



% -------------------------------
%   visibility histogram: experiment
\begin{figure}[h!]
    \centering
    \includegraphics[width=11cm]{pdf/experiment_visibility_histograms.pdf}
    \caption{Histogram of calculated visibility from snapshots of far-field patterns in the three laser experiment where
    the middle laser has applied cavity loss with pump level set to (a) \SI{10}{\percent} \emph{above} and
    (b) \SI{6}{\percent} \emph{below} the lasing threshold.
    (The outer two fibers have fixed pump level at \SI{10}{\percent} above the threshold.)
    The saturation of light in the camera sets an upper limits of the visibility to 0.8.}
    \label{fig:experiment_visibility_histogram}
\end{figure}





%% =============================================================
%%    Simulation
%% =============================================================
\section{Simulation}
\subsection{The multimode model}
We use the same multimode map model as for the all-fiber scheme described in Chapter~\ref{chap:models} to simulate 
the multi-feedback system. The equations read:
\begin{align*}
    E_n^{(m)} (t+T)
    &=
    e^{G_n(t) + i \phi_n^{(m)}}
    \sum_{j=1}^N \left( \mathcal{U}^T \right) {}_{n j}  r_j e^{i \psi_j^{(m)} }
    \sum_{k=1}^N \mathcal{U}_{j k} E_k^{(m)} (t)  + \xi_n^{(m)} (t),  \\
    %%
    G_n(t+T)  &=  G_n(t) + \epsilon \;  f_n \left(G_n(t),  \sum_m \abs{E_n^{(m)} (t)}^2  \right),
\end{align*}
with
\[
	f_n(G, I) = x_n W_\mathrm{th} (G_\mathrm{tot} - G) - (G_\mathrm{tot} +  G) - 2 \left(1 - e^{-2 G} \right) I.
\]
The model is almost identical to the all-in-fiber system in Chapter~\ref{chap:dynamics},
except that additional phase parameters $\psi_j^{(m)}$ are introduced 
to represent the phase shifts accumulated during the propagation in the right arms in the Figure~\ref{fig:weaklink_schematic}.

The multi-feedback ({\it i.e.} multiple outputs contribute in the far-field) feature of the present system is 
incorporated by using nonzero constraints $r_1, \cdots, r_N > 0$, 
each of which corresponds to the reflection by a flat-cleaved fiber end (and extra bending loss).

The coupling matrix $\mathcal{U}$ represents the transfer matrix of the right-propagating wave
for the combination of two 80/20 couplers connecting fiber 1 \& 2 and 2 \& 3, respectively.
Extending the $2\times2$ coupler expression in Equation~\eqref{eq:2x2_coupling_matrix} to three fibers,
the explicit form of  $\mathcal{U}$ is given by
%
\begin{equation}
    \mathcal{U}
    =
    \begin{pmatrix}[0.7]
        \sqrt{0.8}    & i \sqrt{0.2} & 0  \\
         i \sqrt{0.2} &  \sqrt{0.8}  & 0  \\
         0           &  0     &  1
    \end{pmatrix}
    \begin{pmatrix}[0.7]
       1  & 0   & 0            \\
        0  &  \sqrt{0.8}     & i \sqrt{0.2} \\
        0   &  i \sqrt{0.2}  &   \sqrt{0.8}
    \end{pmatrix}
	=
    \begin{pmatrix}[0.7]
        \sqrt{0.8} & i \sqrt{0.2} & 0            \\
         i 0.4     &    0.8       & i \sqrt{0.2} \\
         - 0.2     &  i 0.4       &   \sqrt{0.8}
    \end{pmatrix}.
    \label{eq:coupling_matrix_threelaser}
\end{equation}
%
Reflection coefficients are set to $(r_1, r_2, r_3) = (0.2000, 0.2005, 0.2010)$ to simulate the
even-loss case and $(r_1, r_2, r_3) = (0.200, 0, 0.201)$ to simulate the case of uneven-loss, since in the experiment this case was realized by adding heavy losses to the middle fiber. These values are slightly detuned even in the
``equal loss'' setting to avoid artifacts arising from strict symmetry. Other parameters are set so that
$G_\text{tot} = 2.3$ and $\epsilon = 10^{-3}$. The choice of $\epsilon$ is larger than a physically realistic
value, $\epsilon \sim O(10^-4)$, but one can verify that this only speeds up the convergence without altering
the CW states, as long as it does not induce a numerical instability.

The pumping rates of the outer fibers (Laser \#1 and \#3) are fixed at \SI{10}{\percent} above the lasing threshold (i.e. OFF to CW transition);
so the multipliers are $x_1 = x_3 = 1.1$. The pumping rate of the middle fiber (Laser \#2) is swept from $x_2=0$ to $x_2 
= 2.5$ to replicate the experiment.


%% -----------------------------------
%%
\subsection{Calculation of far-field patterns}
The far-field interference pattern is obtained from the state of the iterative map model by following
procedure. First, the electric fields $E_{\mathrm{facet, } i}^{(m)}$ at each exiting facet $i=1,2,3$
is determined from the state variable $E_i^{(m)}$ (the field at the reference point) via application of the transfer
matrix $\mathcal{U}$ of  wave propagation through the coupler and the output arms:
\[
    E_{\mathrm{out}, i}^{(m)} = \sum_{j=1}^N \mathcal{U}_{i j} E_{j}^{(m)}
\]
where the index $m$ denotes the $m^{th}$ longitudinal mode.
Here $\sum_m \abs{E_{\mathrm{out}, i}^{(m)}}^2$ for $i=1,2,3$
represents the power measured by the experimental taps.

Second, a two-dimensional near-field array output is calculated assuming truncated Gaussian
envelopes positioned at the three fiber core centers and with a cutoff at the core diameters. Each
envelope inherits the amplitude and phase of the propagated electric field.

Third, assuming the Fraunhofer diffraction limit, a two-dimensional far field pattern is computed from the
near field by 2-D Fourier transformation\cite{HechtTextbook}. This is valid because the far field
$E_\mathrm{far}$ is proportional to the 2-D integral,
%
\[
    E_\mathrm{far}^{(m)} (x, y) \propto \iint _\mathrm{Aperture}
        E_\mathrm{near}^{(m)} (x',y') e^{-i k (x x' + y y')/z} dx' dy',
\]
%
and this is practically computed with a 2-D FFT together with a change of length scale.
The irradiance of the instantaneous far field is calculated from sum of the
field intensities over all modes,
\[
    I(x,y) = \sum_m \abs{E_\mathrm{far}^{(m)}}^2 (x, y).
\]

Finally, the time-averaged irradiance pattern is constructed from 20 instantaneous far-field patterns sampled every 5
iterations to mimic the camera exposure time ($\sim 100 \mathrm{\mu s}$) for each captured frame in the experiment.



\subsection{Results}
The change in visibility for varying pump levels in the middle fiber is shown in
Figure~\ref{fig:simulation_visibility_EvenLosses} and
Figure~\ref{fig:simulation_visibility_AddedLoss} for even-loss and uneven-loss setups,
respectively.


% -------------------------------
%   Visibility vs Pump: Simulation
\begin{figure}
    \centering
    \begin{subfigure}[h]{7cm}
        \centering
        \includegraphics[width=6.5cm]{pdf/simulation_visibility_EvenLosses.pdf}
        \caption{Even loss}
        \label{fig:simulation_visibility_EvenLosses}
    \end{subfigure}
    \begin{subfigure}[h]{7cm}
        \centering
        \includegraphics[width=6.5cm]{pdf/simulation_visibility_AddedLoss.pdf}
        \caption{Uneven loss}
        \label{fig:simulation_visibility_AddedLoss}
    \end{subfigure}
    \caption{Average visibility \emph{vs.} middle laser pump level from simulation. Each circle represents a set of
   randomly-generated initial conditions, and the size of a circle corresponds to the number of active modes.}
    \todo[inline]{Size of circles are inconsitent in two graphs. Match them.}   
    \label{fig:simulation_visibility}
\end{figure}


% -------------------------------p
%   Power vs Pump
\begin{figure}
    \centering
    \begin{subfigure}[h]{7.5cm}
        \centering
        \includegraphics[width=7cm]{pdf/simulation_PowerVsPump_EvenLosses.pdf}
        \caption{Even loss}
    \end{subfigure}
    %%
    \begin{subfigure}[h]{7.5cm}
        \centering
        \includegraphics[width=7cm]{pdf/simulation_PowerVsPump_AddedLoss.pdf}
        \caption{Uneven loss}
    \end{subfigure}
    \caption{Output powers vs middle laser pump level from simulation. Outer laser pump levels are fixed at
    1.1x threshold. Uneven losses indicates additional heavy loss to the middle fiber.}
    \label{fig:simulation_power}
\end{figure}
%



%% =============================================================
%%         Discussion
%% =============================================================
\section{Discussion}
%
\subsection{Lack of temporal information in the simulation}
Because our dynamical model is given in map form, the temporal behavior of the output beams are
given by a series of discrete-time snapshots, with a fixed time interval $T$.
The Nyquist-Shanon sampling theorem determines if a continuous signal is recovered from sampled data {\bf Kurt says:  any direct quote ought to have a citation}:
\begin{quote}
A continuous function $x(t)$ is completely determined from data points taken
in every period $\tau$ if $x(t)$ contains no frequencies higher than $1/(2 \tau)$.
\end{quote}
If this condition--called the Nyquist criterion--is met, a continuous function may
be constructed uniquely from sampled data by interpolation.

In fiber lasers, the round-trip time of light is order of \SI{10}{\ns} and hence sets ``the sampling rate'' of the simulation to \SI{10}{\MHz}.
In contrast, the wavelength of ytterbium laser is about \SI{1}{\um}, or of order of \SI{100}{\THz}, enormously surpassing the sampling rate.
Because the Nyquist criterion is not satisfied, the continuous time evolution of the electric field \emph{cannot}
be reconstructed from the simulation data.\footnote{The bandwidth  (> \SI{1}{\nm}) limited by a fiber Bragg grating
is still much larger than the sampling rate hence the undersampling technique, which reconstructs continuous time
information with undersampled data of a bandwidth-limited phenomenon, does not work either in our case.}

This point does not do any harm to the evolution of the dynamical model itself because only snapshots of fields--amplitudes
and phases--matter and the high-frequency information necessary for the model is incorporated in the phase parameters.
Care must be taken, however, when dealing with spatio-temporal patterns of the field outside of the resonators.
The ``fringe patterns'' computed from the simulations represent interference in regions
where the optical path-length differences are of order $c T$: it completely lacks the finite interference
pattern created from path-length differences of (multiples of) the wavelength $\lambda$.
Furthermore, the round-trip time $T$ is of the same order as the coherence time $\tau_c \eqdef 1 / \Delta \nu ~ \SI{10}{\ns}$,
which is the inverse of the frequency bandwidth, because the wavelength is $\lambda ~ \SI{1}{\um}$ and
the bandwidth in a wavelength is $\Delta \lambda ~ \SI{1}{\nm}$.
Therefore, the ``fringes'' from the simulations are physically unobservable patterns
and they are different from the fringes observed in the experiments.

Nevertheless, the ``fringes'' and the corresponding visibility in our simulations still carry some information about coherence
because they directly reflect the phase relations at the laser-emitting fiber facets (although data is taken once every $T$ interval).
When the phases are locked to certain angles, a correlation arises by limiting the possible form of relations.  {\bf Kurt asks:  what ``relations"??}
But the physical realization that satisfies the phase relation is not unique.  Rather, there will be many physical realizations corresponding to a given simulation.
Even if the ``visibility'' from the simulation is unity, a superposition of many realizations
does not give the same visibility, for example.
This discrepancy of the meaning of visibility implies that
visibilities from experiments will be consistently lower than those from simulations.



\subsection{Mode filtering and visibility}
In the all-fiber combination discussed in Chapter~\ref{chap:dynamics},
the single optical feedback (i.e.\ setting of angle-cleaved facet to all but one fibers)
was so strong as to eliminate the longitudinal modes emitting light from the absorbing ports.
In contrast, for multiple optical feedback the 
analogous mode filtering via output coupling alone does not work:  rather,
mode discrimination in both gain and loss are required for effective mode filtering.


This point is observed from the sharp drop in visibility (Figure~\ref{fig:experiment_visibility} and Figure~\ref{fig:simulation_visibility})
when pumping levels are nearly even. Equally pumped fibers are indifferent to modes except for the degree of cross-saturation;
hence, only mode discrimination due to output coupling effectively exists.
Coexistence of multiple frequency modes is observed in this domain according to simulations
Figure~\ref{fig:simulation_visibility}, and no clear phase relations among them emerges.
This is consistent with the experiments (the upper Figure~\ref{fig:experiment_visibility_histogram})
that show low visibility at all times in both even- and uneven-loss cases.


When the middle arm is under-pumped, single-mode selection was occasionally observed in simulations,
giving tiny circles in the Figure~\ref{fig:simulation_visibility}. When the laser is operating with one or only a few frequencies,
output beams are strongly correlated and this results in high visibility. The strongly selected state is \emph{not} globally attracting
in the phase space and we observed dependence on the initial conditions, which in the experiments are uncontrollable.
This behavior is consistent with the experiment shown in the visibility histogram (lower Figure~\ref{fig:experiment_visibility_histogram}).
It has a peak near the camera's saturation limit together with a uniform distribution.
Data points represent attractors of different initial conditions
if the frame rate (\SI{77}{Hz}) of the camera is of the same order as the external noise.
Compared to the exposure time of the camera, the transient of the laser system is negligibly small so that one should be observing the system in one or another attractor over most of the observational time period.


Dragons live in the region where the middle laser is overpumped: it is as yet uncertain what is happening there.
According to the simulations, a fair amount of suppression of active modes occurs,
as visualized by the size of circles in the Figure~\ref{fig:simulation_visibility}.
Furthermore, surviving coexisting modes show a wide variety of correlations (i.e.\ visibilities)
depending on the initial conditions, giving a mid-ranged visibility value on average.
Apparently many attractors coexist in the phase space so that a single ``typical" behavior is hard to identify.
In this domain, more constraints to the system, such as additional losses to enhance mode discrimination,
would be required for creation of coherent beams.


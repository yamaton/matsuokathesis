% !Mode:: "TeX:UTF-8"
% !TEX root = __main.tex

\chapter{Modeling Fiber Laser Array}
\label{chap:models}
%
% ==================================================================
% \section{Cold-Cavity Analysis}
% \label{sec:cold_cavity_analysis}
% One of the most popular quantitative descriptions of laser arrays is a static analysis called ``cold cavity'' analysis.
% As the name suggests, this approach ignores contributions of the gain elements and deals solely 
% with energy dissipation from the system, with the system viewed as a resonator.
% Supermodes, which are resonant modes distributed across the fibers, experience different energy losses in the resonator
% and the low-loss modes are considered to be preferred in the analysis\cite{Mehuys1988}.

% \subsection{A demonstration with a ring laser array}
% We will sketch the cold-cavity analysis for a unidirectional ring laser array.
% Let $\vec{x} \in \mathbb{C}^N$ be the (single-transverse mode) electric fields at a reference cross section of $N$ fibers.
% After traveling around the loop, with a particle picture in mind, the light comes back to the original cross section
% and we describe the evolution of the fields by the map $\vec{x} \to \vec{x'}$.
% Note that the travel times differ depending on the optical paths taken hence
% we switch to the wave picture and treat the time differences as relative phase differences.

% Assuming the amplification is factored out as a positive multiplicative constant $b > 1$,
% and assuming all the other processes---propagation, coupling, internal loss, phase shifts--- are linear,
% we may express the overall effect as a transfer matrix $\mathcal{A}$ multiplied by the constant $b$.
% The map equation reads
% \begin{equation}
%     \vec{x'} = b \mathcal{A} \vec{x}.
%     \label{eq:cold_cavity_simplified}
% \end{equation}
% Considering the fixed point of the map {\bf ?? the steady state is taken to be the fixed point(s) of the map ??} , the eigenvectors of $\mathcal{A}$ are then interpreted as supermodes of the system.
% Since the matrix includes solely processes involving energy loss, the magnitude of the eigenvalues
% $\lambda_1, \lambda_2, \dotsc, \lambda_N$ must be less than unity:
% greater the value $\abs{\lambda} < 1$, lower the energy loss of the corresponding supermode.

% A nontrivial fixed-point solution $\bar{\vec{x}}$ of \eqref{eq:cold_cavity_simplified}
% is possible if and only if the amplification $b$ is tuned properly\footnote{This
% artificial tuning of the parameter is understood physically as a gain saturation effect occurring on a long timescale.}
% to balance the lowest-loss eigenvector contained in the initial condition of $\vec{x}$ such that
% \[
%      b \;  \max \left( \abs{\lambda_1}, \cdots, \abs{\lambda_N} \right)  = 1.
% \]
% Otherwise, $\vec{x}$ either diverges or decays to zero.

% Only the lowest-loss supermode is preferred {\bf does ``preferred" mean ``selected", or something else?} in this simplified picture.
% This tendency remains but does not hold strictly in realistic systems, especially when
% the dynamical model is described in higher dimensions.
% For example, Basov\cite{Basov1965} used the neoclassical laser dynamics equations\cite{SiegmanTextbook},
% where electric fields, polarizations and atomic inversions are treated as dynamical variables,
% and proved that a supermode is stable if its loss is less than the loss in the isolated single-element resonator. {\bf Does this imply that multiple supermodes can simultaneously persist?}

% %%----------------------------------------------------------------------
% \subsection{Failure in multiple gains {\bf Failure of what?}}
% When a resonator contains multiple gain sources,  the amplification of light
% cannot be factored out as a scalar in most situations.
% Instead, the amplification is expressed by a matrix $\mathcal{B}$ so that the
% steady-state equation \eqref{eq:cold_cavity_simplified} is modified to
% \[
%     \bar{\vec{x}} = \mathcal{B} \mathcal{A} \bar{\vec{x}}.
% \]
% The ``cold cavity'' supermodes, or the eigenvectors of $\mathcal{A}$, are no longer useful because
% the loss part alone cannot determine the eigenvalues of the system $\mathcal{B A}$.
% For example, if the fibers are pumped inhomogeneously, which is the case treated in Chapter~\ref{chap:weaklink},
% light amplification is optical path-dependent and this situation occurs {\bf what situation do you mean?}.



% ----------------------------------------------------------------------------
%
\section{Properties of Fiber Lasers}
\subsection{Timescales and adiabatic elimination}
A laser state is typically described by the electromagnetic field $E$,
atomic polarization $P$, and atomic number inversions $N$.
The dynamical behavior of a laser is characterized by the following rates 
corresponding to the dynamical variables: 
the cavity-loss rate $\kappa$,
the relaxation rate of the atomic polarization $\gamma _\perp$,
and the relaxation rate of the population difference $\gamma _\parallel$.
In fiber lasers, these constants are different by orders of magnitude such that
\begin{equation}
	\gamma _\parallel  \ll \kappa  \ll \gamma_\perp.
    \label{eq:caseBcondition}
\end{equation}
Initially noticed by Khokhlov\cite{KhaninMonograph} and widely disseminated by Haken\cite{Haken1980},
a dynamics with fast and slow variables often may be reduced to a lower dimensional
(and often more tractable) dynamics such that slow variables enslave the fast ones, 
which is known as ``adiabatic elimination''\cite{vanKampen1985}.

Under the conditions \eqref{eq:caseBcondition}, 
the fast variable $P$ instantaneously adjusts to changes in the variables $E$ and $N$ which evolve
on a slower (longer) timescale.  The atomic polarization $P$ is ``enslaved'', {\it i.e.} it behaves merely
as a function of the other variables on the slower time scale, so that $\pd{P}{t} = 0$ holds to good approximation.
Consequently, we take the field $E$ and the gain $G \propto N$ as the dynamical variables in constructing
the fiber laser model.

\subsection{Large output coupling}
\label{sub:large_output_coupling}
Laser light is extracted from a resonator as the result of partial reflection at the boundary.
This partial reflection is called ``output coupling'' in the laser community.
Laser models often consider this inevitable output-coupling loss from the resonator as negligible.
The reflectivity of the mirror in a helium-neon laser, for example, is over \SI{99}{\percent},
which is large enough to safely assume lossless reflection and compensate for it by
volumetric ohmic loss. This small loss enables one to enforce resonator modes
as the spatial distribution of the electric field and describe the dynamics via changes of the complex amplitudes of these modes.

By contrast, in fiber lasers the magnitudes of the counter propagating waves in a fiber differ by orders of magnitude
because the flat-cleaved fiber end reflects only about \SI{4}{\percent} of the light back into the resonator.
The on-axis intensity in a steady state varies spatially as the wave passes through the gain medium
and drops instantaneously at the output coupling. Due to this steep spatial build up and drop,
one cannot describe the field in terms of changes in resonator modes.
Hence spatial variations of the field must be incorporated into the dynamical model.

We use single-transverse-mode fibers throughout the thesis, and represent the electric field effectively
by linearly-polarized counter-propagating waves with longitudinal envelopes with Gaussian transversal profile.
 \begin{equation}
      \vec{E} (\vec{r}, t) =
      \hat{x} \left[ A^{(+)}(z, t) e^{i k z} + A^{(-)}(z, t) e^{- i k z}  \right]
      E_\mathrm{Gaussian} (r, \theta) e^{- i \omega t}
      \label{eq:electric_field_representation}
\end{equation}
with $A^{(+)}, A^{(-)}  \in \mathbb{C}$. 
This is valid\footnote{%
This slowly varying envelope approximation (SVEA) is sometimes claimed invalid in fiber lasers\cite{Rogers2005}
due to the large output coupling and the spatial variation of the field. 
SVEA should be still valid, however. The optical wavelength of a fiber laser is
typically centered at \SI{1000}{\nm} and the fiber length is of order \SI{10}{\m},
implying that roughly $10^7$ oscillations are contained during \SIrange{1}{100}{\percent} buildup of the field.
Consequently, the change in the spatial envelope of a wave is much smaller than its oscillation. {\bf This is true, but how does one justify the use of time derivatives replacing the finite differences:  don't SVEA models end up as ODEs?}} when 
the complex amplitude variation is small over the optical wavelength, $L \gg 1/k$.
With this representation in mind, we will develope a dynamical model for the complex amplitudes $A^{(+)}$.


\subsection{Band filtering by a fiber Bragg grating}
\label{sub:bandwidth-filtering}
The spectral bandwidth of a laser is typically determined by the atomic linewidth\cite{Smith1972}.
In a fiber resonator, however, a fiber Bragg grating filters the spectrum by allowing a much narrower bandwidth
than the atomic linewidth, typically less than \SI{1}{\nm} bandwidth. 
Moreover, the longitudinal mode spacing is much narrower than the sliced bandwidth due to the long fiber length (typically a few meters);
hence, tens of thousands of frequency modes are still contained within the Bragg-filtered band.
Because the bandwidth slicing is relatively narrow, the light amplification profile may be considered constant 
(\emph{i.e.\  } independent of the frequency) to the zeroth order.


\subsection{Standing and traveling waves in a fiber}
\label{sub:standing_wave}
Standing waves are formed when counter-propagating waves of the same wavelength and amplitude interfere.
The large output coupling and axial growth of the field within the gain portion of the fiber,
however, make the propagating waves uneven.  
Their interference is especially weak near the output coupler because the incident wave
toward the port is much greater than that of the reflected wave.
Spatial hole burning discussed in Section~\ref{sub:homogeneous_and_inhomogeneous_broadening}
is relatively small due to the dominance of the one-directional traveling waves.

This point is well illustrated by a \emph{static} picture leading to the Rigrod analysis.
Assuming steady-state operation, the governing equations of counter-propagating wave intensities
in a two-mirror resonator with high gain medium are\cite{SiegmanTextbook}
\begin{subequations}
\begin{align}
	\d{I_+(z)}{z} &= + \alpha(z) I_+(z), \\
	\d{I_-(z)}{z} &=  - \alpha(z) I_-(z),
\end{align}
\label{eq:RigrodODE}
\end{subequations}
and the the gain coefficient $\alpha(z)$ is saturated such that\footnote{This gain saturation comes from the two-level scheme
and the specific form is a little different in the three-level system of Yb-doped fibers. But this simplification is good enough to 
illustrate the intensity distribution of counter-propagating waves.}
\[
	\alpha (z) = \frac{\alpha_0}{1 + I_+(z) + I_-(z)}.
\]
%
When one side of the fiber is flat cleaved, giving \SI{4}{\percent} reflectivity, and the other is
perfectly reflecting with use of a fiber Bragg grating, these boundary conditions together with the ODEs
give the axial intensity distribution shown in Figure~\ref{fig:Rigrod}.
%
\begin{figure}[h!]
    \centering
    \includegraphics[width=8cm]{pdf/figure_Rigrod_distribution.pdf}
    \caption{Intensity distribution of right- and left-going waves found by solving \eqref{eq:RigrodODE} 
    with proper boundary conditions. 
    The curves in blue and red are the right-going wave $I_+$ and the left-going wave $I_-$, respectively.
    Intensity is scaled by the maximum value $I_+(L)$.}
   \label{fig:Rigrod}
\end{figure}
%
With this intensity variation, a standing wave is formed only around the perfectly-reflecting side $z=0$ 
while the incident traveling wave to the output port dominates elsewhere in the gain region.




%===========================================================
%  Multi-frequency Model
%===========================================================
\section{Multimode Iterative Map Model}
\subsection{The multimode equations}
We will propose a multi-longitudinal-mode model based on the single-frequency-mode laser model
constructed by Ray \emph{et al}\cite{Ray2008,Ray2009}. The single-mode equations for an $N$-element fiber 
array are given in terms of the evolution of the complex traveling wave amplitude $E$\footnote{In this model 
we may mention $E$ as the ``field'' but it actually means the complex amplitude $A^{(+)}$ defined in 
\eqref{eq:electric_field_representation}.} at a reference point and the spatially averaged 
gain $G$ for each round trip of light in the fibers.  See Figure~\ref{fig:modeling_diagram}.
%
\begin{figure}[h!]
    \centering
    \includegraphics[width=14cm]{pdf/figure_modeling_diagram.pdf}
    \caption{Viewing the state in a coupled fiber as it evolves over one round trip of photon in the cavity,
    the iterative map describes the change in fields and gains after a round trip time.}
   \label{fig:modeling_diagram}
\end{figure}
% ----
\begin{subequations}
    \begin{align}
        \vec{E}(t+T)   &= e^{\mathcal{G}(t) + i \mathcal{\Phi}} \mathcal{U}^\dagger \mathcal{R} \mathcal{U} \vec{E}(t), \\
        G_n(t+T)        &= G_n(t) + \epsilon f \left( \abs{E_n(t)} ^2, G_n(t) \right),
    \end{align}
    \label{eq:singlemode_EOM}
\end{subequations}
% --
where $\vec{E} = \left( E_1, \cdots, E_N \right)  \in \mathbb{C}^N$ and the subscript $n$ is the index of fiber,
 $n = 1 \dots N$. The calligraphic symbols,  $\mathcal{U}, \mathcal{R}, \mathcal{G}(t), \mathcal{\Phi}$, 
 are $N \times N$ matrices.  All but the coupling matrix  $\mathcal{U}$ are diagonal:
\begin{align*}
	\mathcal{G}(t) &= \Diag (G_1(t), \dotsc, G_N(t)),\\
	\mathcal{\Phi} &= \Diag (\phi_1, \dotsc, \phi_N), \\
	\mathcal{R}    &= \Diag (r_1, \dotsc, r_N),
\end{align*}
which represent respectively the light amplifications, phase shifts, and reflection coefficients at the end in the fibers.
The function $f(I, G)$ takes a different form depending on the details of the atomic state transitions\cite{Ray2008}:
\[
	f(I, G) =
	\begin{cases}
		G_p - G - (1 - e^{-2G}) I  & \text{for two-level laser} \\
		w (G_\mathrm{tot} - G) - (G_\mathrm{tot} -G) - 2 (1 - e^{- 2G}) I  & \text{for three-level laser} \\
		w (G_\mathrm{tot} - G) - G - (1 - e^{- 2G}) I               & \text{for four-level laser}
	\end{cases}
\]
where $G_p$ and $w$ are parameters related to the input power and $G_0$ is a parameter for gain saturation.


Based on this single-mode model, we propose a multiple-frequency-mode extension by
introducing $M$ frequency modes (indexed $\mu = 1, \cdots, M$) in the composite cavity.
The multimode equations are written as,
%
\begin{subequations}
    \begin{align}
        \vec{E}^{(\mu)}(t+T)  &= e^{\mathcal{G}(t) + i \mathcal{\Phi}^{(\mu)}}
                                    \mathcal{U}^\dagger \mathcal{R}  \mathcal{U} \vec{E}^{(\mu)} (t), 
       \label{eq:multimode_EOM_field}\\
        G_n(t+T)  &= G_n(t) + \epsilon f \left(\sum_\mu \abs{E_n^{(\mu)} (t)} ^2, G_n(t) \right). 
        \label{eq:multimode_EOM_gain}
    \end{align}
    \label{eq:multimode_EOM}
\end{subequations}
%
Although the electric fields are expanded to higher dimensions (that is, there is one electric field variable for each longitudinal mode), the spatially averaged gain variable $G$ is still treated as a single positive definite quantity for each fiber.
This multimode extension of the model has several underlying assumptions:


%
\begin{itemize}
    \item The spatially averaged gain $G$ is saturated equally by all available longitudinal modes.
    \item The single $G$ assigned to each fiber amplifies the fields of all longitudinal modes.
    \item Optical couplers operate equally regardless of the wave frequency.
\end{itemize}
We will now discuss these points to justify that this heuristic description is sound.


% ---------------------------------------------------------------------------------
\subsection{Rationale for the multimode model}
\label{sub:homogeneous_and_inhomogeneous_broadening}
Resonance of a light amplification medium is often described from two limits of atomic behavior:
if all atoms interact with the incoming light in the same manner,
its spectral line is called homogeneously broadened; conversely, the line is inhomogeneously broadened if the atoms show different responses to a given wave frequency.
The spectral inhomogeneity in solid-state amplifiers is typically caused either by dislocation of atoms from lattice sites,
or crystal impurities. In ytterbium fiber amplifiers, this effect is relatively small
and the spectral line is for all practical purposes considered homogeneously broadened\cite{Paschotta1997}.

Although an ytterbium amplifier is spectrally homogeneous, it is \emph{spatially} inhomogeneous.
This is caused by standing waves in a two-mirror resonator.
The waves interact with atoms strongly at anti-nodes which makes ``holes'' of gain saturation,
a situation called ``spatial hole burning''. Node locations are of course slightly different for each mode. 
Hence a localized gain interacts with modes differently, leading to spatial inhomogeneity.


Typically, multimode operation in ytterbium lasers is understood from two-step processes:
\begin{enumerate}
	\item A homogeneously broadened atomic lineshape supports a single frequency in the ideal limit\cite[Chap.~12]{SiegmanTextbook}
	due to mode competition\footnote{Mode competition is described only qualitatively or statically in most of the literature.
	Clarification of the phenomenon from a dynamical perspective is one of the topics discussed in Chapter~\ref{chap:dynamics}.},
	caused by the frequency dependent profile of the atomic line, which is typically Lorentzian in form.
	%%
	\item The preferred mode consumes gain by burning ``holes'' spatially. Other modes
	are still supported by the unburned gain near the nodes of the preferred mode.
\end{enumerate}
%
However, as discussed in Section~\ref{sub:bandwidth-filtering}, we are assuming a flat atomic lineshape, 
implying that mode competition does not occur in this limit (due to the lack of a frequency dependent atomic line).
The modes are differentiated by their standing-wave locations only.
The spatially averaged gain $G$ appears to interact with all modes equally
because the spatially extended gain is burned uniformly over all.
This is consistent with the single valued $G$ in each fiber interacting 
with the longitudinal modes equally in the Equation \eqref{eq:multimode_EOM_gain}.


Note that standing waves effectively exist only near the perfect mirror (i.e. fiber Bragg grating) due to the large output coupling,
as seen in the Figure~\ref{fig:Rigrod}, and traveling waves dominate most of the resonator.
The effect of the spatial hole burning supporting multiple frequencies is hence small. 
Indeed, Wu \emph{et al}\cite{Wu2010} neglected the backward-propagating waves in a linear resonator and
transformed the problem into an equivalent ring laser of unidirectional waves.
This ring-laser description allows localized gains to be shared by different frequency modes
via cross saturation and strengthens the mode competition, typically.
Hence electric fields are amplified by the same shared gain $G$ in a fiber regardless of
their mode indices $(\mu)$ in Equation \eqref{eq:multimode_EOM_field}.
In the standing-wave picture, in contrast, a wave of a certain frequency interacts with 
the corresponding localized gains in its antinodes hence the equation \eqref{eq:multimode_EOM_field} does not hold.


\subsection{Frequency-independence of a fiber coupler}
The amount of coupling in an optical fiber coupler is generally dependent on the frequency of light,
as seen in Section~\ref{sec:couplers}. However, the coupling strength varies on a scale of 
tens of nanometers in optical wavelength, while the bandwidth filtered by a fiber Bragg grating
is much narrower--less than one nanometer in wavelength.  Due to this scale difference,
an optical coupler may be treated effectively as a frequency independent instrument. 
Hence the same transfer matrix, represented by $\mathcal{U}$, 
applies to all longitudinal modes in Equation~\eqref{eq:multimode_EOM}.



\subsection{Stability of multimode operation of the multimode model}
As reviewed previously, fiber lasers allow multimode operation as a result of the interplay between
local mode competition and distributed gain consumption.
The mode competition, which tends to prohibit coexistence of modes locally, 
is promoted by the homogeneously broadened, frequency dependent atomic lines.
Meanwhile, distributed resource consumption, which tends to \emph{promote} the coexistence of modes,
is supported by standing waves and spatial hole burning, one of the standard mechanism of multi-longitudinal mode 
operation.

In the multimode model, however, both factors are absent.  
The effects of homogeneous broadening is completely neutralized due to the assumption of a flat atomic lineshape.
Interference of counter-propagating waves is also dropped from the multimode model 
partially because it (carelessly) borrowed the derivation of the single-mode model\cite{Rogers2005}, where interference is simply ignored,
and partially because the interference is largely supressed due to the dominance of one-directional
propagating wave shown in the Figure~\ref{fig:Rigrod}. 

Although these approximations are well grounded, we should bear in mind that 
they make the present model more marginal with respect to the multimode operation by
removing two factors, one encouraging and one discouraging the coexistence of modes.



